/*
 Navicat Premium Data Transfer

 Source Server         : 8.130.31.18
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : 8.130.31.18:3306
 Source Schema         : cd

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 18/02/2023 18:13:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for consult
-- ----------------------------
DROP TABLE IF EXISTS `consult`;
CREATE TABLE `consult`  (
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RId` int(0) NOT NULL,
  `Contime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `Condetails` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ReplyContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `Replytime` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`SId`, `RId`, `Contime`) USING BTREE,
  INDEX `FK_RId`(`RId`) USING BTREE,
  CONSTRAINT `consult_ibfk_1` FOREIGN KEY (`RId`) REFERENCES `recruitment` (`RId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `consult_ibfk_2` FOREIGN KEY (`SId`) REFERENCES `student` (`SId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of consult
-- ----------------------------
INSERT INTO `consult` VALUES ('20101020201', 1, '2022-05-13 23:12:32', '教育规划', '考研、留学、高考', '2022-05-28 19:55:02');
INSERT INTO `consult` VALUES ('20101020201', 5, '2022-05-13 23:47:08', '如何实现职业转换的技巧', '性格倾向的程度', '2022-05-27 19:55:08');
INSERT INTO `consult` VALUES ('20101020201', 6, '2022-05-13 23:46:59', '如何实现职业转换的技巧', '性格倾向的程度', '2022-05-19 19:55:14');
INSERT INTO `consult` VALUES ('20101020201', 7, '2022-05-13 23:47:41', '如何实现职业转换的技巧', '性格倾向的程度', '2022-05-18 19:55:19');
INSERT INTO `consult` VALUES ('20101020202', 1, '2022-05-13 23:14:18', '职业如何发展、瓶颈如何突破', '考研、留学、高考', '2022-04-13 19:55:41');
INSERT INTO `consult` VALUES ('20101020202', 5, '2022-05-13 23:17:05', '感觉很迷茫，不知道该做啥？', '好好读书才是正道', '2022-09-01 19:55:36');
INSERT INTO `consult` VALUES ('20101020202', 6, '2022-05-13 23:19:10', '工作就是打杂，这样有发展吗？', '条条大路通罗马', '2022-05-11 19:55:49');
INSERT INTO `consult` VALUES ('20101020203', 6, '2022-05-13 23:27:20', '工作就是打杂，这样有发展吗', '加油，你是最棒的', '2022-04-25 19:56:59');
INSERT INTO `consult` VALUES ('20101020203', 7, '2022-05-13 23:29:09', '瞎忙学不到东西被淘汰怎么办？', '加油，你是最棒的', '2022-05-24 19:55:55');
INSERT INTO `consult` VALUES ('20111020205', 3, '2022-05-13 23:49:28', '感觉很迷茫，不知道该做啥？ ', '好好学习基础知识，打好基础！', '2022-05-28 20:17:13');
INSERT INTO `consult` VALUES ('20111020205', 5, '2022-05-13 23:49:01', '感觉很迷茫，不知道该做啥？ ', '好好读书才是正道', '2022-07-01 19:56:49');
INSERT INTO `consult` VALUES ('20111020205', 8, '2022-05-13 23:50:01', '感觉很迷茫，不知道该做啥？ ', '好好读书才是正道', '2022-05-03 19:56:00');
INSERT INTO `consult` VALUES ('20111120203', 6, '2022-05-13 23:51:50', '工作就是打杂，这样有发展吗   ', '条条大路通罗马', '2022-05-09 19:56:30');
INSERT INTO `consult` VALUES ('20201020201', 5, '2022-05-13 23:40:24', '如何实现职业转换的技巧', '性格倾向的程度', '2022-04-12 19:56:07');
INSERT INTO `consult` VALUES ('20201020201', 6, '2022-05-13 23:40:42', '如何实现职业转换的技巧', '性格倾向的程度', '2022-05-19 19:55:26');
INSERT INTO `consult` VALUES ('20201020202', 2, '2022-05-13 23:34:48', '如何根据现有工作经验、教育背景，寻找更适合的职业或岗位', '在职业定位的基础上，结合咨询者的工作经验、所受教育、性格倾向的程度', '2022-05-02 19:56:24');
INSERT INTO `consult` VALUES ('20201020202', 3, '2022-05-13 23:33:54', '如何根据现有工作经验、教育背景，寻找更适合的职业或岗位', '在职业定位的基础上，结合咨询者的工作经验、所受教育、性格倾向的程度', '2022-10-01 19:56:13');
INSERT INTO `consult` VALUES ('20201020202', 5, '2022-05-13 23:39:24', '如何根据现有工作经验、教育背景，寻找更适合的职业或岗位', '在职业定位的基础上，结合咨询者的工作经验、所受教育、性格倾向的程度', '2022-05-02 19:56:36');
INSERT INTO `consult` VALUES ('20201020202', 7, '2022-05-13 23:30:55', '瞎忙学不到东西被淘汰怎么办？', '那就集中注意力学一个', '2022-05-02 19:56:43');

-- ----------------------------
-- Table structure for delivery
-- ----------------------------
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE `delivery`  (
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RId` int(0) NOT NULL,
  `Astate` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`SId`, `RId`) USING BTREE,
  INDEX `FK_RId`(`RId`) USING BTREE,
  CONSTRAINT `FK_RId` FOREIGN KEY (`RId`) REFERENCES `recruitment` (`RId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_SId` FOREIGN KEY (`SId`) REFERENCES `student` (`SId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of delivery
-- ----------------------------
INSERT INTO `delivery` VALUES ('20101020201', 3, 1);
INSERT INTO `delivery` VALUES ('20101020201', 4, 0);
INSERT INTO `delivery` VALUES ('20101020201', 6, 0);
INSERT INTO `delivery` VALUES ('20101020201', 9, 0);
INSERT INTO `delivery` VALUES ('20101020205', 4, 1);
INSERT INTO `delivery` VALUES ('20111020205', 1, 1);
INSERT INTO `delivery` VALUES ('20111020205', 2, 1);
INSERT INTO `delivery` VALUES ('20111020205', 3, 1);
INSERT INTO `delivery` VALUES ('20111020205', 4, 1);
INSERT INTO `delivery` VALUES ('20111020205', 5, 1);
INSERT INTO `delivery` VALUES ('20111120204', 1, 0);
INSERT INTO `delivery` VALUES ('20111120204', 2, 0);
INSERT INTO `delivery` VALUES ('20111120204', 3, 0);
INSERT INTO `delivery` VALUES ('20111120204', 4, 1);
INSERT INTO `delivery` VALUES ('20111120204', 5, 0);
INSERT INTO `delivery` VALUES ('20111120204', 6, 0);
INSERT INTO `delivery` VALUES ('20111120204', 7, 0);
INSERT INTO `delivery` VALUES ('20111120204', 8, 1);
INSERT INTO `delivery` VALUES ('20201020201', 6, 0);

-- ----------------------------
-- Table structure for dynamic
-- ----------------------------
DROP TABLE IF EXISTS `dynamic`;
CREATE TABLE `dynamic`  (
  `DId` int(0) NOT NULL AUTO_INCREMENT,
  `DContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DTitle` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '动态标题',
  PRIMARY KEY (`DId`) USING BTREE,
  INDEX `Fk_SId`(`SId`) USING BTREE,
  CONSTRAINT `dynamic_ibfk_1` FOREIGN KEY (`SId`) REFERENCES `student` (`SId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dynamic
-- ----------------------------
INSERT INTO `dynamic` VALUES (1, '不上岗不收费', '2022-05-12 20:34:52', '20101020201', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (2, '降低企业人才招聘成本，解决专业人才求职难题。', '2022-05-12 20:39:26', '20101020202', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (3, '解决专业人才求职难题。', '2022-05-12 20:53:31', '20201020202', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (4, '简化HR招聘工作。', '2022-05-12 20:55:01', '20111120204', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (5, '企业高管直接与人才交流。', '2022-05-12 20:55:45', '20111120203', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (6, '助人才上岗优质岗位是Offer派的目标。', '2022-05-12 20:56:38', '20111120204', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (7, '以人才为中心 。', '2022-05-12 20:57:24', '20201020202', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (8, ' 以结果为导向。', '2022-05-12 20:58:43', '20101020201', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (9, ' 以社区为基础的一览offer派。', '2022-05-12 20:59:10', '20101020202', '个人求职简历高效制作方法');
INSERT INTO `dynamic` VALUES (10, '一、近年来我国大学毕业生的总体就业形势\r\n\r\n　　大学生就业难是一个现实问题，更是一个社会问题。社会主义市扬经济体制的建立和发展，产业结构的不断优化升级，正猛烈地冲击着我国的高等教育，大学生就业在社会转型期遇到了很大的挑战，总体就业形势不容乐观。\r\n\r\n　　1.就业人数庞大，就业高峰持续时间长，形势严峻\r\n\r\n　　自从2000年以来，毕业生的人数每年都在增加，2011年毕业生的人数是2001年的5倍多。据教育部统计，2004年全国普通高校毕业生280万，截至当年9月1日，全国普通高校毕业生平均就业率达到73%；2005年毕业338万大学生，截至当年9月1日，全国高校毕业生就业率为72.6%；2006年全国应届大学毕业生人数激增至413万，全国大学生就业率为76.69%。2007年全国普通高校毕业生人数达到495万，平均就业率达到70%；2008年全国毕业生人数为559万，大学生就业率为70%；2009年高校毕业生611万，大学毕业生初次就业率达到74%；2010年的大学生毕业生630万，截至7月1日，高校毕业生就业率为72.2%；2011年大学毕业生人数达660万，在2012年全国教育工作会议上，刘延东表示，2011年普通高校毕业生离校时初次就业率达到77.8%，连续9年保持在70%以上；2012年全国普通高校毕业生规模将达680万人，700多万名中专、职高、技校等也将集中进入就业市场，应届毕业生的就业问题仍将会集中显现①。\r\n\r\n　　从图中可以看出：近几年来，全国高校毕业生人数逐年剧增，加上往年沉淀下来的毕业生，大学生总体就业形势一年比一年严峻。虽然这几年全国高校毕业生就业率基本持平，但由于毕业生人数逐年剧增，所以绝对数在增加。可以预见，在未来相当长时期内大学生就业压力不会减弱。', '2022-11-24 22:16:43', '20101020205', '大学生就业现状');
INSERT INTO `dynamic` VALUES (18, '一、近年来我国大学毕业生的总体就业形势\r\n\r\n　　大学生就业难是一个现实问题，更是一个社会问题。社会主义市扬经济体制的建立和发展，产业结构的不断优化升级，正猛烈地冲击着我国的高等教育，大学生就业在社会转型期遇到了很大的挑战，总体就业形势不容乐观。\r\n\r\n　　1.就业人数庞大，就业高峰持续时间长，形势严峻\r\n\r\n　　自从2000年以来，毕业生的人数每年都在增加，2011年毕业生的人数是2001年的5倍多。据教育部统计，2004年全国普通高校毕业生280万，截至当年9月1日，全国普通高校毕业生平均就业率达到73%；2005年毕业338万大学生，截至当年9月1日，全国高校毕业生就业率为72.6%；2006年全国应届大学毕业生人数激增至413万，全国大学生就业率为76.69%。2007年全国普通高校毕业生人数达到495万，平均就业率达到70%；2008年全国毕业生人数为559万，大学生就业率为70%；2009年高校毕业生611万，大学毕业生初次就业率达到74%；2010年的大学生毕业生630万，截至7月1日，高校毕业生就业率为72.2%；2011年大学毕业生人数达660万，在2012年全国教育工作会议上，刘延东表示，2011年普通高校毕业生离校时初次就业率达到77.8%，连续9年保持在70%以上；2012年全国普通高校毕业生规模将达680万人，700多万名中专、职高、技校等也将集中进入就业市场，应届毕业生的就业问题仍将会集中显现①。\r\n\r\n　　从图中可以看出：近几年来，全国高校毕业生人数逐年剧增，加上往年沉淀下来的毕业生，大学生总体就业形势一年比一年严峻。虽然这几年全国高校毕业生就业率基本持平，但由于毕业生人数逐年剧增，所以绝对数在增加。可以预见，在未来相当长时期内大学生就业压力不会减弱。', '2022-11-24 22:17:31', '20101020205', '大学生就业现状');
INSERT INTO `dynamic` VALUES (20, '\r\n\r\n日子如同白驹过隙，迎接我们的将是新的工作机会，新的挑战，这时候需要开始写简历了哦。好的简历都具备一些什么特点呢?以吓是小编为大家整理的个人求职简历高效制作方法，仅供参考，欢迎大家阅读。\r\n\r\n1.目标叙述过于华丽或平常\r\n\r\n许多候选人在履历的开始部分的目标叙述时就让人兴趣寡然。最糟糕的目标叙述一般是这样开始的: \"一个具挑战性的职位不仅让我有机会为公司做贡献而且也给我以成长和进步的机会。”这样的叙述早已用滥掉了，而且太过平常，浪费了宝贵的履历空间。如果你正在写履历，试试用小纸条来代替目标叙述，在小纸条上你可以说说你的工作或你专长的领域。\r\n\r\n2.过短和过长\r\n\r\n太多的人想把他们的经历压缩在一页纸上，因为他们曾经听说履历最好不要超过一页。当将履历格式化地缩到一页时，许多求职者就删除了他们给人深刻印象的成就。反之亦然。就拿那些在履历上几页纸漫谈不相干的或者冗长的经历的候选人来说，看的人很容易就会觉得无聊。所以，当你写履历时，试着问自己: \"这些陈述会让我得 到面试的机会吗?”然后,仅仅保留那些会回答”是” 的信息。\r\n\r\n3.决定履历篇幅是否恰当的规则就是没有定则\r\n\r\n决定其篇幅的因素包括有职业、企业、工作经历、教育和造诣程度等等。重要的就是，履历中的每一个字都要能够推销该候选人。\r\n\r\n4.人称代词和冠词的用法\r\n\r\n履历是商业沟通的形式，它应该是简洁和被正式书写的。它不应该出现\"我”的字样，尽少用冠词。试看一例：\r\n\r\n陈述：我已经开发了一种新的产品，它使销售额增加了200万，使部分销路总量增加了12%.\r\n\r\n应该写成：新开发的产品使销售额增加了200万,使部分销路总量增加了12%.\r\n\r\n5.罗列私人信息或者不相干的信息\r\n\r\n许多人会在履历中概括他们的兴趣，比如阅读，徒步旅行和滑雪等等。其实，这些只有在它们与目标工作有关联的时候才最好加入。例如，候选人申请的是一份滑雪教练的工作，那么他或她就应该提到其喜欢乡间滑雪的兴趣。\r\n\r\n6.当你有很好的职业记录时选用功能履历\r\n\r\n人事经理曾经说过,一个讨厌的麻烦就是有的候选人只是描述他的或她的技能而不涉及具体工作。对每一个职位都虎视眈眈是让人讨厌的。除非你的履历有紧急情况，比如说事实上没有工作记录或者极端繁忙工作，你要避免使用格式化的功能履历。最有效果的一种格式是经过改良的按年代排列的类型。请看一下其基本版面安排；页眉(姓名,住址，电子邮件地址，电话号码)；以有力的个人概述引领（详细设计你的主要经历和专业领域）；年代次序颠倒的雇佣记录（着重于过去10到15年的成就）教育（为了有所梯度，该项可以移到顶部）；另外的相关主题包括职业联盟,社会活动，技术专长，出版物，专利和所用语言等。\r\n\r\n7.没有摘要或没有概括部分使最初的自我推销发生困难\r\n\r\n摘要部分是求职者最好的工具。做过该项内容的候选人就会知道这种类型的技能和资格占有很重要的地位。摘要中应该说明与所求职位相关的技能等级和经验。为了创造一个有影响力的概括陈述，老练的开头会决定什么才是对雇主们来说是重要的。接下来，罗列一下你匹配的技能、经验和教育情况。这些自我推销点就可以并入概括部分了。\r\n', '2022-11-26 13:27:21', '20101020201', '个人求职简历高效制作方法');

-- ----------------------------
-- Table structure for enter_operation
-- ----------------------------
DROP TABLE IF EXISTS `enter_operation`;
CREATE TABLE `enter_operation`  (
  `EId` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '企业号',
  `MId` int(0) NOT NULL COMMENT '管理员号',
  `Eope` int(0) NULL DEFAULT 0 COMMENT '操作内容：0：无操作；1：删除；2：屏蔽；3：修改',
  PRIMARY KEY (`EId`, `MId`) USING BTREE,
  INDEX `MId`(`MId`) USING BTREE,
  CONSTRAINT `enter_operation_ibfk_1` FOREIGN KEY (`EId`) REFERENCES `enterprise` (`EId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `enter_operation_ibfk_2` FOREIGN KEY (`MId`) REFERENCES `manager` (`MId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enter_operation
-- ----------------------------
INSERT INTO `enter_operation` VALUES ('3202002105301', 5, 0);
INSERT INTO `enter_operation` VALUES ('3202002105301', 6, 2);
INSERT INTO `enter_operation` VALUES ('3202002105302', 5, 1);
INSERT INTO `enter_operation` VALUES ('3202002105303', 6, 1);
INSERT INTO `enter_operation` VALUES ('3202002105304', 6, 2);
INSERT INTO `enter_operation` VALUES ('3202002105305', 6, 3);
INSERT INTO `enter_operation` VALUES ('3202002105306', 7, 0);
INSERT INTO `enter_operation` VALUES ('3202002105307', 8, 0);
INSERT INTO `enter_operation` VALUES ('3202002105308', 8, 0);

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise`  (
  `EId` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '企业号',
  `EPwd` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '123456' COMMENT '密码',
  `ECur` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '企业简介',
  `EName` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '企业名称',
  `EPrinName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人姓名',
  `EPosition` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人职位',
  `Estate` int(0) NOT NULL DEFAULT 0 COMMENT '状态',
  `EImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '帐号.png' COMMENT '头像',
  PRIMARY KEY (`EId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprise
-- ----------------------------
INSERT INTO `enterprise` VALUES ('3202002105301', '123456', '软件开发、电子动漫研发、销售；游戏推广；广告设计、制作；计算机业务咨询；电子商务.', '成辰网络科技有限公司', '程晨', '人事部员工', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105302', 'abcdef', '互联网技术开发、技术服务、技术咨询、技术转让；计算机产品设计；计算机系统集成；设计、制作、代理、发布国内外广告；展览展示服务；利用互联网销售', '重庆小飞象科技有限公司', '王勇', '人事部员工', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105303', 'abcdef', '组织文化艺术交流活动；其他文化艺术经纪代理；专业设计服务；平面设计；企业管理咨询；企业管理；信息咨询服务（不含许可类信息咨询服务）；企业形象策划；广告制作；广告设计、代理；广告发布；摄像及视频制作服务；摄影扩印服务；会议及展览服务；品牌管理；技术服务、技术开发、技术咨询、技术交流、技术转让、技术推广；5G通信技术服务；信息技术咨询服务。', '宜宾联鸿文化传媒有限公司', '刘元彪', 'HR', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105304', 'abcdef', '老板使用58招人神器发布该职位，公司招人的诚意大到无需描述，赶紧来应聘吧。', '成都玉足记忆健康管理有限公司', '张燕', '人事部副总', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105305', 'abc123', NULL, '宜宾蓝腾物流有限公司', '杨毅', '老板', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105306', 'abc11111', NULL, '宜宾聚会文化传媒有限公司', '孟邦宇', '老板', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105307', 'abc113451', '①宜宾*健身游泳连锁品牌，发展空间大； ②目前在运营的大型综合性健身游泳场馆三家，两年内至少再开5家大型门店，急缺英才； ③服务至上的运营理念，即将*服务体系，需要志同道合的千里马一起开创未来； ③全智能化SAAS系统配合植入，结合智能化外接设备，健身数据全掌握，宜宾*可进化的智慧场馆； ④以人为本的企业文化，每季度开展储备管理人员选拔，手把手扶上岗，重视员工发展通道建设，重视释放人才的潜能和价值，只等你来。', '宜宾市叙州区时代名人', '王胜莲', 'HR', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105308', 'abcdef', '公司主要经营射钉紧固器材及其零配件、塑料制品、工模具、化工机械及其零配件及货物进出口业务。', '宜宾伊瑞达紧固件有限公司', '吴琼', 'HR', 0, '帐号.png');
INSERT INTO `enterprise` VALUES ('3202002105334', '123456', NULL, '宜宾', NULL, NULL, 0, '帐号.png');

-- ----------------------------
-- Table structure for job
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job`  (
  `JId` int(0) NOT NULL AUTO_INCREMENT,
  `Jname` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`JId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job
-- ----------------------------
INSERT INTO `job` VALUES (1, '软件工程师');
INSERT INTO `job` VALUES (2, '会计');
INSERT INTO `job` VALUES (3, 'UI工程师');
INSERT INTO `job` VALUES (4, '新媒体运营');
INSERT INTO `job` VALUES (5, '文案策划员');
INSERT INTO `job` VALUES (6, '电子工程师');
INSERT INTO `job` VALUES (7, '律师');
INSERT INTO `job` VALUES (8, '前端工程师');
INSERT INTO `job` VALUES (9, '产品经理');
INSERT INTO `job` VALUES (10, 'IT系统工程师');
INSERT INTO `job` VALUES (11, '网络工程师');
INSERT INTO `job` VALUES (12, '校招');
INSERT INTO `job` VALUES (13, '游戏特效师');
INSERT INTO `job` VALUES (14, '金融保险专业实习');
INSERT INTO `job` VALUES (15, '2023春季校招');

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `MId` int(0) NOT NULL,
  `MName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MPwd` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MPhone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '帐号.png' COMMENT '头像\r\n头像',
  PRIMARY KEY (`MId`) USING BTREE,
  UNIQUE INDEX `MPhone`(`MPhone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, 'm1', '123456', '17703771999', '帐号.png');
INSERT INTO `manager` VALUES (2, 'm2', '123456', '15388889881', '帐号.png');
INSERT INTO `manager` VALUES (3, 'm3', '123456', '18083815777', '帐号.png');
INSERT INTO `manager` VALUES (4, 'm4', '123456', '15377892222', '帐号.png');
INSERT INTO `manager` VALUES (5, 'm5', '123456', '18168526111', '帐号.png');
INSERT INTO `manager` VALUES (6, 'm6', '123456', '15310952123', '帐号.png');
INSERT INTO `manager` VALUES (7, 'm7', '123456', '15321168157', '帐号.png');
INSERT INTO `manager` VALUES (8, 'm8', '123456', '13357024777', '帐号.png');

-- ----------------------------
-- Table structure for policy
-- ----------------------------
DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy`  (
  `PResource` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_german2_ci NOT NULL,
  `PTime` timestamp(0) NOT NULL,
  `PContent` text CHARACTER SET utf8mb3 COLLATE utf8mb3_german2_ci NOT NULL,
  `PId` int(0) NOT NULL,
  `PTitle` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_german2_ci NOT NULL,
  PRIMARY KEY (`PId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of policy
-- ----------------------------

-- ----------------------------
-- Table structure for recruitment
-- ----------------------------
DROP TABLE IF EXISTS `recruitment`;
CREATE TABLE `recruitment`  (
  `RId` int(0) NOT NULL AUTO_INCREMENT,
  `RTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '招聘信息主题',
  `RContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `Rkick` int(0) NOT NULL DEFAULT 0,
  `Rstate` int(0) NOT NULL DEFAULT 0 COMMENT '1:删除，0：未删除',
  `EId` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `JId` int(0) NULL DEFAULT NULL,
  `Salary` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '薪资',
  `Degree` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学历',
  `Address` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工作地址',
  `RCount` int(0) NULL DEFAULT NULL COMMENT '招聘人数',
  `Rpicture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'pic1 (4).jpeg' COMMENT '宣传图片',
  PRIMARY KEY (`RId`) USING BTREE,
  INDEX `FK_EId`(`EId`) USING BTREE,
  INDEX `FK_JId`(`JId`) USING BTREE,
  CONSTRAINT `FK_EId` FOREIGN KEY (`EId`) REFERENCES `enterprise` (`EId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_JId` FOREIGN KEY (`JId`) REFERENCES `job` (`JId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of recruitment
-- ----------------------------
INSERT INTO `recruitment` VALUES (1, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '职位要求：\r\n1、全日制统招本科及以上学历；\r\n2、具备较好的文字功底，对网络有一定的喜好；\r\n3、熟练并会使用office 软件，具备基本的网络知识；\r\n4、富有责任心、具有团队精神；具备良好的学习能力，抗压能力强\r\n', '2022-05-12 21:22:24', 79, 1, '3202002105303', 4, '8000-10000', '专科及以上', '四川省成都市', 2, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (2, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '1.电子信息工程相关专业，电子类知识基础扎实。2.熟悉两款以上单片机，并有完整的项目经验，用C语言编写硬件驱动程序，有良好的编程风格。', '2022-05-12 21:24:19', 91, 0, '3202002105301', 6, '8000-10000', '专科及以上', '四川省成都市', 5, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (3, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '具备独立思考或独立工作能力，能主动独立完成诉讼及非诉工作； 5、完成合伙人安排的其他 1、法学本科及以上学历，持有律师执业证三年以上经验； 2、具有非诉讼经验与争议解决领域的出庭经验； 3、具有敬业精神、团队精神、保密意识，能承受工作压力，逻辑思维能力、理解力、沟通协调能力较强； 工作任务。', '2022-06-24 15:04:34', 31, 1, '3202002105308', 3, '8000-10000', '专科及以上', '四川省成都市', 23, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (4, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '1.学历专业：全日制统招本科及以上学历；机电、自动化和计算机相关专业；', '2022-05-12 21:28:53', 29, 0, '3202002105308', 6, '8000-10000', '本·科及以上', '四川省成都市', 4, 'pic1 (3).jpeg');
INSERT INTO `recruitment` VALUES (5, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。\r\n职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。\r\n职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。\r\n职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。\r\n职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。\r\n职位描述：\r\n能做一般纳税人帐，独立申报税，开票及认证进项票,核算工资，勤快，有责任心。', '2022-05-12 21:32:26', 89, 1, '3202002105307', 2, '8000-10000', '专科及以上', '四川省成都市', 6, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (6, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '岗位职责:\r\n1、根据产品设计原型开发前端代码；\r\n2、利用HTML/CSS/JavaScriptjQuery等各种Web技术进行网页PC端界面的前端构建；\r\n3、参与Web前端架构设计，解决技术难点，解决不同的浏览器及不同版本的兼容性问题；\r\n4、JavaScript、jQuery等交互原型的制作。\r\n5、负责Web前端页面开发，负责页面交互的技术设计、开发、代码优化等；\r\n6、精通DIV+CSS网页框架布局的HTML代码编写，熟悉W3C标准；\r\n7、熟悉JavaScript、CSS、JQuery等各种Web前端开发技术，具备一定的跨浏览器开发经验；\r\n8、熟悉各主流浏览器（IE6+、Firefox、Chrome、Safari）间的差异性，能快速定位和解决各种兼容难题；\r\n', '2022-05-12 21:34:08', 120, 0, '3202002105302', 3, '8000-10000', '硕士及以上', '四川省成都市', 7, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (7, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '岗位职责:\r\n1、根据产品设计原型开发前端代码；\r\n2、利用HTML/CSS/JavaScriptjQuery等各种Web技术进行网页PC端界面的前端构建；\r\n3、参与Web前端架构设计，解决技术难点，解决不同的浏览器及不同版本的兼容性问题；\r\n4、JavaScript、jQuery等交互原型的制作。\r\n5、负责Web前端页面开发，负责页面交互的技术设计、开发、代码优化等；\r\n6、精通DIV+CSS网页框架布局的HTML代码编写，熟悉W3C标准；\r\n7、熟悉JavaScript、CSS、JQuery等各种Web前端开发技术，具备一定的跨浏览器开发经验；\r\n8、熟悉各主流浏览器（IE6+、Firefox、Chrome、Safari）间的差异性，能快速定位和解决各种兼容难题；\r\n', '2022-05-12 21:36:20', 24, 0, '3202002105306', 3, '8000-10000', '专科及以上', '四川省成都市', 9, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (8, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '\r\n\r\n岗位职责：\r\n1、公司的业务网站上的和公众号等平台的内容文案撰写编辑、网站和公众号上更新维护，主要是产品硬软广告推文。。主要是文字水平，洗稿，伪原创水平。\r\n2、外部媒体平台，b乎，百家号，头条，搜狐号，微信公众号等部分运营执行和策划; 参与公司业务市场分析和策划。\r\n3、 听党指挥，我是革命一块砖，哪儿需要哪儿搬。能与公司相关业务部门紧密配合协助，制订推广运营计划策略。以及可能的公司里的其他涉及到文字创意的工作。\r\n', '2022-05-12 21:38:27', 169, 0, '3202002105303', 5, '8000-10000', '专科及以上', '四川省成都市', 12, 'pic1 (7).jpeg');
INSERT INTO `recruitment` VALUES (9, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '1、能够根据项目软件需求说明和相应的软件设计文档实现软件产品；\r\n 2、从事具体的技术方案编写，软件架构设计，软件代码的编写，软件测试，还有一些文档性文件的编写，能够独立承担项目。\r\n 3、维护后期的软件升级及相应售后服务。\r\n 任职要求：\r\n 1、学历专业：全日制统招本科及以上学历；机电、自动化和计算机相关专业；\r\n 2、工作经验：3年以上相关工作经验；\r\n 3、精通C#或JAVA语言，开发CS或BS模式的开发，设计软件架构，编写软件代码。\r\n 4、具有Oracle，SQL Server数据库的使用经验，熟练进行SQL查询操作。\r\n5、能够运用编程语言实现以太网或串口协议的通讯技术，有与PLC以太网或串口通讯经验者优先。\r\n 6、具有利用WinFrom开发自动化上位机软件或应用程序经验。\r\n 7、如有在自动化行业从事相关上位机软件的开发优先考虑。\r\n8、如有MES（制造执行系统）相关咨询、研发、调试、使用者优先考虑。\r\n 9、具有良好的团队精神和良好的沟通能力，有激情，能够承受一定的压力下工作，能够适应出差。\r\n10、事业合伙人，股份激励.', '2022-05-28 16:18:32', 29, 0, '3202002105301', 1, '8000-10000', '专科及以上', '四川省成都市', 14, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (10, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '1、根据产品设计原型开发前端代码；', '2022-05-28 16:52:55', 13, 0, '3202002105307', 3, '8000-10000', '专科及以上', '四川省成都市', 12, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (11, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '2、利用HTML/CSS/JavaScriptjQuery等各种Web技术进行网页PC端界面的前端构建；', '2022-05-28 16:52:55', 11, 0, '3202002105308', 3, '8000-10000', '本科及以上', '四川省成都市', 11, 'pic1 (8).jpeg');
INSERT INTO `recruitment` VALUES (12, 'test', '3、参与Web前端架构设计，解决技术难点，解决不同的浏览器及不同版本的兼容性问题；', '2022-05-28 16:52:55', 34, 1, '3202002105303', 3, '8000-10000', '专科及以上', '四川省成都市', 65, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (18, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '\r\n\r\n岗位职责：\r\n\r\n1. 参与与策略相关的信息搜集，协助完成创意策略；\r\n\r\n2. 广告主题、策划/规则制订、MINISITE、小游戏、H5等相关文案创意发想；微信、微博日常维护；\r\n\r\n3. 文字整理及撰写，支持策略提案；\r\n\r\n4. 协助公司其他工作安排。\r\n\r\n\r\n\r\n\r\n任职资格：\r\n\r\n1. 熟悉广告创作流程，有敏锐的创作嗅觉和精准的创意表现力；\r\n\r\n2. 具备独立思考和分析能力，对问题有自己独特的见解；\r\n\r\n3. 能够准确捕捉产品的亮点，具备恰如其分的文字表现能力；\r\n\r\n4. 有全案策划的成功案例，市场触觉敏锐，逻辑思维能力强；\r\n\r\n5. 工作心态积极向上，善于团队协作；\r\n\r\n6. 广告公司同等职位1年以上工作经验，具有互动经验行业者优先考虑。', '2022-06-21 14:44:40', 1, 0, '3202002105302', 1, '6000-8000', '本·科及以上', '四川省成都市', 33, 'pic1 (3).jpeg');
INSERT INTO `recruitment` VALUES (19, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '岗位职责：\r\n\r\n1、根据项目需要策划并制定部门微信、微博等新媒体的运营策略，策划并执行推广活动；\r\n\r\n2、制定目标任务，通过活动、社群、文章内容等手段完成KPI考核，建设有效运营手段提升用户活跃度；\r\n\r\n3、具有一定的数据分析能力，提取数据报表，分析推广效果并优化；\r\n\r\n4、参与公司整体品牌、产品的营销策划与市场推广；\r\n\r\n5、负责对外PR工作，拓展并维护外部媒体资源关系网络，与KOL建立长期友好关系，建立媒体档案库。\r\n\r\n任职要求：\r\n\r\n1、中文系或新闻传播相关专业本科及以上学历；\r\n\r\n2、3年以上新媒体（微博、微信等）文案撰写经验，熟悉网络化表达方式；\r\n\r\n3、文字编辑、创作能力突出，能独立完成原创内容稿件撰写和伪原创内容编辑；\r\n\r\n4、善于站在用户的角度思考问题，写出解决用户痛点的文字；\r\n\r\n5、善于捕捉互联网热点事件与话题，对网络语言敏感度高，思维活跃、有创新意识和挑战精神；\r\n\r\n6、经常混迹于朋友圈、知乎、微博、天涯、豆瓣等各类互联网的论坛/社群，有较强的文案策划能力，能对社会化话题进行发想创作；\r\n\r\n7、有广告公司文案经历或媒体（自媒体或大号）相关工作经验者优先，\r\n\r\n8、热爱互联网行业，勇于创新，知识面广，思维活跃，创意优，能快速响应社会、行业热点话题，对整合传播、新闻传播、活动与事件管理均有深刻的认识与经验。', '2022-06-21 14:49:32', 1, 0, '3202002105303', 4, '7000-12000', '专科及以上', '四川省成都市', 2, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (20, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '岗位职责：\r\n\r\n1、根据项目需要策划并制定部门微信、微博等新媒体的运营策略，策划并执行推广活动；\r\n\r\n2、制定目标任务，通过活动、社群、文章内容等手段完成KPI考核，建设有效运营手段提升用户活跃度；\r\n\r\n3、具有一定的数据分析能力，提取数据报表，分析推广效果并优化；\r\n\r\n4、参与公司整体品牌、产品的营销策划与市场推广；\r\n\r\n5、负责对外PR工作，拓展并维护外部媒体资源关系网络，与KOL建立长期友好关系，建立媒体档案库。\r\n\r\n任职要求：\r\n\r\n1、中文系或新闻传播相关专业本科及以上学历；\r\n\r\n2、3年以上新媒体（微博、微信等）文案撰写经验，熟悉网络化表达方式；\r\n\r\n3、文字编辑、创作能力突出，能独立完成原创内容稿件撰写和伪原创内容编辑；\r\n\r\n4、善于站在用户的角度思考问题，写出解决用户痛点的文字；\r\n\r\n5、善于捕捉互联网热点事件与话题，对网络语言敏感度高，思维活跃、有创新意识和挑战精神；\r\n\r\n6、经常混迹于朋友圈、知乎、微博、天涯、豆瓣等各类互联网的论坛/社群，有较强的文案策划能力，能对社会化话题进行发想创作；\r\n\r\n7、有广告公司文案经历或媒体（自媒体或大号）相关工作经验者优先，\r\n\r\n8、热爱互联网行业，勇于创新，知识面广，思维活跃，创意优，能快速响应社会、行业热点话题，对整合传播、新闻传播、活动与事件管理均有深刻的认识与经验。', '2022-06-21 14:54:04', 2, 0, '3202002105308', 4, '7000-12000', '专科及以上', '四川省成都市', 3, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (21, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '岗位职责： 1. 参与迭代式的前端开发, 高质量还原设计稿; 2. 参与设计人员对产品的交互和体验设计, 根据反馈并做出调整。 任职要求： 1. 2023年毕业，统招本科及以上学历，计算机相关专业； 2. 了解数据结构和算法基础知识； 3. 掌握面向对象编程思想； 4. 熟练掌握前端基础知识(HTML/CSS/Java script)及相关规范; 5. 掌握Vue/React/Angular其中至少一种框架，并能够熟练使用于开发； 6. 熟练使用版本管理工具git； 7. 至少掌握一门css预编译语言，sass，less或stylus； 8. 具备较强的责任意识和服务意识； 9. 良好的沟通能力，对前端技术有强烈的好奇心，始终关注前端前沿技术。 【加分项】 1. 掌握Type script; 2. 熟悉前端工程化(Webpack/Grunt/Gulp BDD/TDD,CI); 3. 了解node基础知识', '2022-06-21 19:43:25', 0, 0, '3202002105308', 1, '6000-8000', '专科及以上', '四川省成都市', 50, 'pic1 (3).jpeg');
INSERT INTO `recruitment` VALUES (22, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '请输入职位信息', '2022-06-24 15:04:46', 0, 0, '3202002105308', 1, '6000-8000', '专科及以上', '四川省成都市', 2, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (23, '[全职]——中国联合网络通信有限公司肇庆市分公司招聘IT系统工程师', '3、参与Web前端架构设计，解决技术难点，解决不同的浏览器及不同版本的兼容性问题；', '2022-11-25 19:44:58', 1, 0, '3202002105303', 3, '8000-10000', '专科及以上', '四川省成都市', 65, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (24, '金融保险专业实习+500强企业可盖章+4000', '长白班室内办公，不外跑，公司提供客户资源，更有智能AI系统，百分百接通率，节假日休息\r\n23届实习生入职可提供实习证明。\r\n一、岗位内容；\r\n工作内容1、负责解答客户疑问，维护公司客户关系\r\n2、通过95522系统平台对泰康客户进行通知类、咨询类、推荐类服务。\r\n3、公司产品上下线的及时通知，并新活动、新产品推荐给客户。\r\n岗位要求\r\n1. 普通话标准 沟通能力好，有耐心。\r\n2 .21-35周岁，大专及以上学历，优秀者可中专高中。\r\n工资福利及福利待遇：\r\n考勤时间时间：早8:30到12:00；下午13:30到18:00，午休1.30小时；\r\n试用期:底薪2800+津贴+10%奖金提成+500/1000/1500优增奖金，综合工资范围5000-8000元左右；\r\n2、国家假日休息，享受婚假、产假、丧假、哺乳假、公司不定期组织旅游方案、团建活动等团体活动；\r\n3、提供五险一金\r\n工作地点：\r\n职场：桥西中山路友谊大街交口北行200米路东，地铁1号线直达，交通方便长白班室内办公，不外跑，公司提供客户资源，更有智能AI系统，百分百接通率，节假日休息', '2022-11-26 20:24:40', 3, 0, '3202002105308', 14, '4000-8000', '专科及以上', '河北省石家庄市', 15, 'pic1 (2).jpeg');
INSERT INTO `recruitment` VALUES (25, '金融保险专业实习+500强企业可盖章+4000', '见校园双选会。', '2023-02-12 20:19:33', 0, 0, '3202002105308', 15, '2000-15000', '大学本科', '四川省宜宾市五粮液公司', 102, 'pic1 (2).jpeg');

-- ----------------------------
-- Table structure for resume
-- ----------------------------
DROP TABLE IF EXISTS `resume`;
CREATE TABLE `resume`  (
  `RsId` int(0) NOT NULL AUTO_INCREMENT COMMENT '简历编号',
  `Revaluation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '自我评价',
  `Rsphone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '电话',
  `Rsexper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '实习经历',
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Jname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无',
  `RBirth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '出生年月',
  `REducation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学历',
  `RStudyTime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '就读时间',
  `RsImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '证件照',
  `RAddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '现居地',
  `REmail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `RHonor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '奖励荣誉',
  `RCertificate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '证书',
  `RLesson` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '未填写' COMMENT '主修课程',
  PRIMARY KEY (`RsId`) USING BTREE,
  UNIQUE INDEX `Rsphone`(`Rsphone`) USING BTREE,
  INDEX `Fk_SId1`(`SId`) USING BTREE,
  INDEX `Fk_JId1`(`Jname`) USING BTREE,
  CONSTRAINT `resume_ibfk_2` FOREIGN KEY (`SId`) REFERENCES `student` (`SId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of resume
-- ----------------------------
INSERT INTO `resume` VALUES (1, '软件开发', '17162659655', '', '20101020203', '会计', '2002.09', '本科', '2023.06', 'R-C.jpg', '无', '', '', '无', '未填写');
INSERT INTO `resume` VALUES (2, '计算机应用基础、应用文写作、数学、英语、德育、电工与电子技术、计算机网络技术、C语言、计算机组装与维修、企业网安全高级技术、企业网综合管理、windows server 2008操作系统、局域网组建、Linux服务器操作系统、网络设备与网络技术（主要学习思科、华为公司设备的配置、管理、调试）、SQL Server、网络综合布线技术、CAD绘图等。\r\n\r\n', '15082372019', '无', '20101020201', '嵌入式开发工程师', '2000.01', '专科及以上', '2019.09-2023.06', 'R-C.jpg', '河北省石家庄市', '3093416686@qq.com', '无', '无', '计算机应用基础、应用文写作、数学、英语、德育、电工与电子技术、计算机网络技术、C语言、计算机组装与维修、企业网安全高级技术、企业网综合管理、windows server 2008操作系统、局域网组建、Linux服务器操作系统、网络设备与网络技术（主要学习思科、华为公司设备的配置、管理、调试）、SQL Server、网络综合布线技术、CAD绘图等。\r\n\r\n');
INSERT INTO `resume` VALUES (3, '数据分析,社群经营,活动策划', '18162659875', '', '20101020202', '会计', '2000.03', '本科', '2023.06', 'R-C.jpg', '无', '', '', '无', '未填写');
INSERT INTO `resume` VALUES (4, '文案写作', '17172648644', '', '20111120203', '会计', '2001.04', '本科', '2023.06', 'R-C.jpg', '无', '', '', '无', '未填写');
INSERT INTO `resume` VALUES (6, '前端设计', '17172659655', '', '20101020205', '会计', '2001.04', '本科', '2023.06', 'R-C.jpg', '无', '', '', '无', '未填写');
INSERT INTO `resume` VALUES (7, '积极好学，善于学习新事物', '15082373728', '无', '20111020205', '会计', '1999.09', '本科', '2023.06', 'R-C.jpg', '无', '1234098712@qq.com', '无', '无', '未填写');
INSERT INTO `resume` VALUES (8, '无', '12345678790', '无', '20111120204', '前端工程师', '1999.07', '本科', '2023.06', 'R-C.jpg', '无', '1234098712@qq.com', '无', '无', '未填写');

-- ----------------------------
-- Table structure for stu_operation
-- ----------------------------
DROP TABLE IF EXISTS `stu_operation`;
CREATE TABLE `stu_operation`  (
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '学生号',
  `MId` int(0) NOT NULL COMMENT '管理员号',
  `Sope` int(0) NOT NULL DEFAULT 0 COMMENT '操作内容：0：无操作；1：删除；2：屏蔽；3：修改',
  PRIMARY KEY (`SId`, `MId`) USING BTREE,
  INDEX `MId`(`MId`) USING BTREE,
  CONSTRAINT `stu_operation_ibfk_1` FOREIGN KEY (`SId`) REFERENCES `student` (`SId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `stu_operation_ibfk_2` FOREIGN KEY (`MId`) REFERENCES `manager` (`MId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stu_operation
-- ----------------------------
INSERT INTO `stu_operation` VALUES ('20101020201', 1, 2);
INSERT INTO `stu_operation` VALUES ('20101020201', 2, 1);
INSERT INTO `stu_operation` VALUES ('20101020202', 2, 2);
INSERT INTO `stu_operation` VALUES ('20101020203', 2, 3);
INSERT INTO `stu_operation` VALUES ('20101020205', 3, 3);
INSERT INTO `stu_operation` VALUES ('20111020204', 1, 0);
INSERT INTO `stu_operation` VALUES ('20111020205', 1, 1);
INSERT INTO `stu_operation` VALUES ('20111120203', 2, 1);
INSERT INTO `stu_operation` VALUES ('20111120204', 3, 2);
INSERT INTO `stu_operation` VALUES ('20201020201', 4, 0);
INSERT INTO `stu_operation` VALUES ('20201020202', 4, 0);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `SId` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `SName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `SNickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `SAge` int(0) NULL DEFAULT NULL,
  `SPwd` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '123456',
  `DName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '专业',
  `SPhone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SSc` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Sstate` int(0) NOT NULL DEFAULT 0,
  `Ssex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `SSp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '专业',
  `SImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '头像.png' COMMENT '\'头像.png\'',
  PRIMARY KEY (`SId`) USING BTREE,
  INDEX `DId`(`DName`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('20101020201', '张三', '一 一', 20, '123456', '计算机技术', '13550096656', '四川轻化工大学', 0, '男', '计算机', 'a.jpg');
INSERT INTO `student` VALUES ('20101020202', '李四', '看透', 20, '12345', '计算机技术', '18162659875', '四川轻化工大学', 0, '女', '计算机', '头像.png');
INSERT INTO `student` VALUES ('20101020203', '王五', '鲤鱼', 21, '12345', '软件工程', '17162659655', '四川轻化工大学', 0, '男', '软件', '头像.png');
INSERT INTO `student` VALUES ('20101020205', '张顺', '不善', 20, '12345', '软件工程', '17172659655', '四川轻化工大学', 0, '女', '软件', '头像.png');
INSERT INTO `student` VALUES ('20111020204', '周易', '泣鬼神', 20, '12345', '物联网', '17172658654', '四川轻化工大学', 0, '女', '物联网', '头像.png');
INSERT INTO `student` VALUES ('20111020205', '蔡荣', '情书', 20, '12345', '电子信息技术', '17172658655', '四川轻化工大学', 0, '男', '物联网', '头像.png');
INSERT INTO `student` VALUES ('20111020215', '许一一', '一 一', 0, '123456', '计算机技术', NULL, '四川轻化工大学', 0, '女', NULL, '头像.png');
INSERT INTO `student` VALUES ('20111120203', '朱从', '一梦', 20, '12345', '测绘工程', '17172648644', '四川轻化工大学', 0, '男', '电商', '头像.png');
INSERT INTO `student` VALUES ('20111120204', '莫莫', '安之', 20, '12345', '机械工程', '17172648654', '四川轻化工大学', 0, '女', '电商', '头像.png');
INSERT INTO `student` VALUES ('20201020201', '卓知', '丫头', 20, '12345', '车辆工程', '17162659855', '四川轻化工大学', 0, '女', '测控', '头像.png');
INSERT INTO `student` VALUES ('20201020202', '何发', '黄松', 20, '12345', '车辆工程', '17162659875', '四川轻化工大学', 0, '男', '测控', '头像.png');

SET FOREIGN_KEY_CHECKS = 1;
