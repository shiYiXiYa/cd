<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>学生信息管理</title>
        <link rel="stylesheet" href="../css/student.css">
    </head>

    <body>
        <div class="top">
            <div class="top_box">
                <div class="top_left">
                    <h1>大学生就业服务平台</h1>
                </div>
                <div class="top_right">
                    <img src="../images/帐号.png" alt="">
                    <h4>帐号XXX</h4>
                    <div class="top_right_end"></div>
                    <div class="top_droplist">
                        <a href="#">帐号管理</a>
                        <a href="#">退出登录</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="content"> -->
        <div class="left">
            <div class="left_box">
                <div class="left_box_top">
                    <img src="../images/帐号.png" alt="">
                    <h3>帐号</h3>
                </div>
                <div class="left_box_a">
                    <a href="#">学生信息管理</a>
                    <a href="#">企业信息管理</a>
                    <a href="#">招聘信息管理</a>
                    <a href="#">就业政策管理</a>
                </div>
            </div>
        </div>
        <div class="right">
            <div class="right_box">
                <div class="right_box_top">
                    <div class="in_box">
                        <form action="">
                            <input type="submit" value="查询">
                            <input type="search" name="" id="">
                            <input type="button" value="批量删除">
                            <input type="button" value="导入学生信息">
                        </form>
                    </div>
                </div>
                <div class="right_box_table">
                    <form action="">
                        <table>
                            <tr>
                                <th><input type="checkbox" name="check" id="check_all">全选</th>
                                <th>编号</th>
                                <th>学号</th>
                                <th>姓名</th>
                                <th>学校</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="check" id=""></td>
                                <td>1</td>
                                <td>19101020234</td>
                                <td>张三丰</td>
                                <td>四川轻化工大学</td>
                                <td>
                                    <select name="" id="">
                                    <option value="">正常</option>
                                    <option value="">封禁</option>
                                </select>
                                </td>
                                <td>
                                    <button>删除</button>
                                    <span>|</span>
                                    <a href="">修改</a>
                                </td>
                            </tr>
                            <tr class="table_bottom">
                                <td colspan="7">
                                    <button>首页</button>
                                    <button>&lt;</button> 第
                                    <span>1</span> 页
                                    <button>&gt;</button>
                                    <button>尾页</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <!--<div class="right_box_bottom"></div>-->
            </div>
        </div>
        <!-- </div> -->

    </body>

    </html>