<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>头部和尾部</title>
     <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/dynamic-list.css">
    <style type="text/css">
    .dynamic-left .flexitem a {
        color: #252525;
    }
    #submit{
    	border: none;
    }
    </style>
</head>

<body>
    <!-- 最上端的头部 -->
    <%@ include file="hander.jsp" %>
    <!-- 动态列表 -->
    <div id="dynamic">
        <div class="dynamic-left">
            <ul>
                <li class="header"><span>热门动态</span></li>
                <c:forEach var="item" items="${sessionScope.page_dynamics}">
              <li>
                    <div class="flexitem"><img src="${pageContext.request.contextPath }/images/${item.getStudent().getSImg() }" alt="">
                        <div><a href="${pageContext.request.contextPath }/dynamic?action=display&id=${item.getDId()}">
                            <p class="title">${item.getDTitle()}</p>
                            <p class="Dcontent text-truncate" style="max-width: 500px;">${item.getDContent()}</p></a>
                            <div class="nickname"><label>${item.student.getSNickname()}</label> <!-- <span>浏览量：234</span> --><span>${item.getDTime()}</span></div>
                        </div>
                    </div>
                </li>
				</c:forEach>
            </ul>
            <!-- 分页 -->
            <form action="${pageContext.request.contextPath }/fenye" id="fenye">
            <input type="hidden" value="dynamic" name="action">
            <input type="hidden" value="1" name="yema" id="yema">
             <input type="hidden" value="1"  id="flag"> 
            <div class="fenyebtn">
                <ul>
                    <li>共<span>${requestScope.total_pages} </span>页</li>
                    <li>第<span>${requestScope.current_page} </span>页</li>
                    <li><button>首页</button></li>
                    <li><button>上一页</button></li>
                    <li><button>下一页</button></li>
                    <li><button>尾页</button></li>
                </ul>
            </div>
            </form>
            <div class="clearfloat"></div>
        </div>
        <div class="dynamic-right">
            <div class="launch">
                <form id="searchdynamics" action="${pageContext.request.contextPath }/dynamic">
                <input type="text" name="search">
                <input type="hidden" name="action" value="search">
                    <button id="submit" ><img  src="${pageContext.request.contextPath }/images/search.png" alt=""></button>
                </form>
                <a href="${pageContext.request.contextPath }/views/dynamic-release.jsp" class="a-launch">发布动态 </a>
            </div>
            <div class="clearfloat"></div>
        </div>
        <div class="clearfloat"></div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script>
    	if(<%=request.getAttribute("total_pages")%>==null){
    		$("#fenye").submit();
    	}
    	<%--console.log(<%=session.getAttribute("page_dynamics")%>);--%>
    	console.log(<%=request.getAttribute("total_pages")%>);<%--注释--%>
    	console.log(<%=request.getAttribute("current_page")%>);
        let value =<%=request.getAttribute("current_page")%>;
        if (value == 1) {
        	$(".fenyebtn li button:first").attr("disabled", "disabled");
            $(".fenyebtn li button").eq(1).attr("disabled", "disabled");
        }
        if(value ==<%=request.getAttribute("total_pages")%> ){
        	  $(".fenyebtn li button").eq(2).attr("disabled", "disabled");
              $(".fenyebtn li button:last").attr("disabled", "disabled");
        }
        $(".fenyebtn li").on("click", function() {
           // console.log($(this).index());
            //console.log($("button").eq($(this).index() - 2));
            let value = $("#yema").val();
            // 首页
            if ($(this).index() === 2) {
                $("#yema").val("1");
                $(".fenyebtn li button:first").attr("disabled", "disabled");
                $(".fenyebtn li button").eq(1).attr("disabled", "disabled");
                $("#fenye").submit();
                // 上一页
            } else if ($(this).index() === 3) {
                let value = <%=request.getAttribute("current_page")%>;
                value--;
                console.log("上一页："+value);
                $("#yema").val(value);
                if (<%=request.getAttribute("current_page")%> === 1) {
                    $(".fenyebtn li button:first").attr("disabled", "disabled");
                    $(".fenyebtn li button").eq(1).attr("disabled", "disabled");
                }
                $("#fenye").submit();
                // 下一页
            } else if ($(this).index() === 4) {
                let value =<%=request.getAttribute("current_page")%>;
                value++;
                console.log("下一页："+value);
                $("#yema").val(value);
                if (<%=request.getAttribute("current_pages")%> == <%=request.getAttribute("total_pages")%>) {
                    $(".fenyebtn li button").eq(2).attr("disabled", "disabled");
                    $(".fenyebtn li button:last").attr("disabled", "disabled");
                }
                $("#fenye").submit();
            } else {
                $("#yema").val(<%=request.getAttribute("total_pages")%>);
                $(".fenyebtn li button").eq(2).attr("disabled", "disabled");
                $(".fenyebtn li button:last").attr("disabled", "disabled");
                $("#fenye").submit();
            }
        })
        
        $("#submit").on("click",function(){
        	$("#searchdynamics").submit();
        })
    </script>
</body>

</html>