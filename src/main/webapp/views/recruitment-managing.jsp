<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>招聘信息管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/recruitmentmanage.css">
    <style type="text/css">
    .tixing{
    	color:navy;
    	text-align: center;
    	font-size:20px;
    	margin-top:140px;
    	font-weight: 700;
    }
    .list{
 		margin-top: 30px;
 	}
    </style>
</head>

<body>
    <!-- 最上端的头部 -->
 	<%@ include file="hander.jsp" %>
    <!-- 主要内容 -->
    <div class="container main" style="min-height: 600px">
        <div class="row">
            <div class="col-3 m1 border"><a href="${pageContext.request.contextPath }/recruitment?action=manager&EId=${sessionScope.enterprise.getEId()}">招聘信息管理</a></div>
            <div class="col-3 m1">
           <!-- 
            <c:if test="${sessionScope.recruitments!=null }">
            <a href="${pageContext.request.contextPath }/resume?action=findjob&EId=${sessionScope.enterprise.getEId()}">简 历 管 理</a>
			</c:if>    
			 -->  
			 <a href="${pageContext.request.contextPath }/resume?action=findjob&EId=${sessionScope.enterprise.getEId()}">简 历 管 理</a>
            </div>
        </div>
        <!-- 招聘信息 -->
        <div class="list">
            <div class="row">
                <div class="col-1"></div>
                <%-- ${sessionScope.enterprise.getEId()} --%> 
                <div class="col-3 insert"><a  href="${pageContext.request.contextPath }/views/recruitment-release.jsp">新增招聘信息</a></div>
                <div class="col-3"></div>
                <div class="col-4 iform">
                    <form action=""><input type="text"><button type="submit">搜索</button></form>
                </div>
            </div>
            
          <c:if test="${sessionScope.recruitments==null }">
            	<div class="zhaopinlist" style="height: 300px; margin-top: 15px;border:solid 1px #2525;">
              		<div class="tixing">你还没有发布过招聘信息!快去发布吧！</div>
              	</div>
              </c:if>
               <c:if test="${sessionScope.recruitments!=null }">
               <div class="zhaopinlist">
                <table>
                    <thead>
                        <tr>
                            <!-- 				含招聘信息编号、发布时间、岗位类型，状态 -->
                            <th>RID</th>
                            <th>岗位</th>
                            <th>点击数</th>
                            <th>发布时间</th>
                            <th>详情</th>
                            <th>状态</th>
                            <th>操作</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${sessionScope.recruitments }" var="item">
                        <tr>
                            <td>
                                ${item.getRId()}
                            </td>
                            <td>
                                ${item.getJob().getJname() }
                            </td>
                            <td>
                                ${item.getRkick() }
                            </td>
                            <td>
                                ${item.getRTime() }
                            </td>
                            <td class="">
                                <div class="text-truncate" style="max-width: 700px;">
                                    ${item.getRContent() }
                                </div>
                            </td>
                             <c:choose>
                                <c:when test="${item.getRstate()==0}">
	                            	<td>无</td>
	                            	 <td>
	                            	 <%-- ${sessionScope.enterprise.getEId()} --%>
	                                <a href="${pageContext.request.contextPath }/recruitment?action=obtain&RId=${item.getRId()}"><button>修改</button></a>
	                                <a href="${pageContext.request.contextPath }/recruitment?action=delete&RId=${item.getRId()}&state=1&EId=${sessionScope.enterprise.getEId()}"><button>删除</button></a>
	                           		 </td>
                                </c:when>
                                <c:otherwise>
                                	<td>已删除</td>
	                            	 <td>
	                                <a href="${pageContext.request.contextPath }/recruitment?action=delete&RId=${item.getRId()}&state=0&EId=${sessionScope.enterprise.getEId()}"><button>找回</button></a>
	                           		 </td>
	                           		 </c:otherwise>
	                           		
                                </c:choose>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
               
            </div>
             </c:if>
        </div>
        <!-- 简历 -->
        <div class="resume">
            <!-- 岗位列表 -->
            <c:choose>
             <c:when test="${sessoionScope.joblistOptions!=null }">
            	暂未发布任何招聘信息!
            </c:when> 
            <c:otherwise>
            <div class="diyihang">
                <div class="row1">
                <form action="${pageContext.request.contextPath }/resume" id="manageForm">
			        <input type="hidden" name="action" value="manager">
			        <input type="hidden" name="EId" value="${sessionScope.enterprise.getEId()}">
			        <label for="">投递岗位：</label>
			        <select name="JId" id="selectManage">
			        <c:choose>
			        <c:when test="${requestScope.delivery_select!=null }">
			       		 <option value="${requestScope.delivery_select}">${requestScope.delivery_select}</option>
			        </c:when>
			        <c:otherwise>
                    <option value="请下拉选择招聘岗位">请下拉选择招聘岗位</option>
                    </c:otherwise>
                    </c:choose>
                    <!-- 重新设置工作岗位列表 joblistOptions -->
                    <!--
                    <c:forEach items="${sessionScope.joblist}" var="item">
                  <option  value="${item.getJId() }">${item.getJname() }</option> 
                  </c:forEach>
                  -->
                  <c:forEach items="${sessionScope.joblistOptions}" var="item">
                  <option  value="${item.getJId() }">${item.getJname() }</option> 
                  </c:forEach>
                </select>
                
    </form>
                </div>
                <form action="" class="isearch">
                    <input type=""><button type="submit">搜索</button></input>
                </form>
            </div>
            <div class="rlist">
            <c:choose>
            <c:when test="${requestScope.delivery_notice!=null }">
            	<div class="deliverynotice">${requestScope.delivery_notice}</div>
            </c:when>
            <c:otherwise>
                <section>
                    <table>
                        <thead>
                            <tr>
                                <!-- 				（含招聘信息编号，岗位类型，学号，姓名，学校，专业，录取状态） -->
                                <th>招聘信息编号</th>
                                <!-- <th>岗位</th> -->
                                <th>学生姓名</th>
                                <th>学校</th>
                                <th>专业</th>
                                <th>录取状态</th>
                                <th>操作</th>
                                <th>查看详情</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${sessionScope.deliveryList}">
                                <tr>
                                    <td>${item.getRId()}</td>
                                   <%--  <td>${item.job.getJname()}</td> --%>
                                    <td>${item.student.getSName()}</td>
                                    <td>${item.student.getSSc()}</td>
                                    <td>${item.student.getDName()}</td>
                                    <c:choose>
                                    <c:when test="${item.getAstate()==1}">
                                    <td>已录取</td>
                                    <td>无法修改</td>
                                    </c:when>
                                    <c:otherwise>
                                    <td>暂无</td>
                                     <td><a href="${pageContext.request.contextPath }/resume?action=change&rid=${item.getRId()}&sid=${item.getSId()}&state=0&JId=${requestScope.delivery_selectid}&EId=${sessionScope.enterprise.getEId()}"><button>修改录取状态</button></a></td>
                                    </c:otherwise>
                                    </c:choose>
                                    <td><a href="${pageContext.request.contextPath }/resume?action=display&sid=${item.student.getSId()}">点击查看</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </section>
                </c:otherwise>
			</c:choose>
            </div>
            </c:otherwise>
            </c:choose>
             
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script>
    	if(<%=session.getAttribute("joblistOptions")!=null%>){
    		$(".m1").toggleClass("border");
	        $(".list").css("display", "none");
	        $(".resume").css("display", "block");
    	}else{
    		$(".list").css("display", "block");
	        $(".resume").css("display", "none");
    	}
        $(".m1").on("click", function() {
            console.log($(this).index());
            let i = $(this).index();
            $(".m1").toggleClass("border");
            if (i === 0) {
            	<%request.removeAttribute("joblistOptions");%>
                $(".list").css("display", "block");
                $(".resume").css("display", "none");
            } else {
                $(".list").css("display", "none");
                $(".resume").css("display", "block");
            }
        })
        $("#selectManage").on("change", function() {
            console.log("yes");
            $("#manageForm").submit();
        })
        $(".praise").attr("disabled",true).css("pointer-events","none");  
    </script>
</body>

</html>