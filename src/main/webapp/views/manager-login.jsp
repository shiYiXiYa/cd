<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/login.css">
    <title>管理员登录界面</title>
</head>

<body>
    <div class="left-content">
        <h1>大学生就业服务平台</h1>
        <h4>管理系统</h4>
    </div>
    <div style="text-align:center" id="box">
        <form action="${pageContext.request.contextPath }/manager" meta="post" id="managerlogin">
            <input name="action" type="hidden" value="managerlogin">
            <h3>管理员登录</h3>
            <div class="inputbox">
                账号： <input type="text" name="mid"><br> 密码：
                <input type="password" name="mpwd"><br>
            </div>
            <div class="inputbox">
                <button>登 录</button>
        </form>
        </div>
</body>
<style>
    #box {
        position: relative;
        background-color: rgba(173, 197, 207, 0.536);
        border-radius: 16px;
        /* margin: 0px auto;
        margin-top: 50px;
        padding-left: 10px; */
        height: 550px;
        top: 10vh;
        left: 63%;
    }
    
    #box h3 {
        padding-top: 45px;
    }
    
    #box .inputbox input {
        background-color: #f4f8f9;
    }
    
    .left-content {
        position: absolute;
        top: 185px;
        /* display: none; */
        left: 12px;
    }
    
    .left-content h1 {
        letter-spacing: 63px;
        margin-left: 30px;
        font-size: 2.8rem;
        color: #64d5f5;
    }
    
    .left-content h4 {
        letter-spacing: 88px;
        font-size: 4rem;
        margin-left: 250px;
        margin-top: 65px;
        color: #6bbdd3;
    }
</style>

</html>