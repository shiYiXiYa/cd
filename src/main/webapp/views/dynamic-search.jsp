<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>头部和尾部</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/dynamic-list.css">
     <style type="text/css">
    .dynamic-left .flexitem a {
        color: #252525;
    }
    #submit{
    	border: none;
    }
    </style>
</head>

<body>
    <!-- 最上端的头部 -->
    <%@ include file="hander.jsp" %>
    <!-- 动态列表 -->
    <div id="dynamic">
        <div class="dynamic-left">
            <ul>
                <c:if test="${sessionScope.searchDynamics!=null}">
                <c:forEach var="item" items="${sessionScope.searchDynamics}">
              <li>
                    <div class="flexitem"><img src="/images/${item.getStudent().getSImg() }" alt="">
                        <div><a href="${pageContext.request.contextPath }/dynamic?action=display&id=${item.getDId()}">
                            <p class="title">${item.getDTitle()}</p>
                            <p class="Dcontent text-truncate" style="max-width: 500px;">${item.getDContent()}</p></a>
                            <div class="nickname"><label>${item.student.getSNickname()}</label> <!-- <span>浏览量：234</span> --><span>${item.getDTime()}</span></div>
                        </div>
                    </div>
                </li>
				</c:forEach>
				</c:if>
				<c:if test="${sessionScope.searchDynamics==null}">
				<li>本次搜索结果为空！</li>
				</c:if>
            </ul>
            <div class="clearfloat"></div>
        </div>
        <div class="dynamic-right">
            <div class="launch">
                 <form id="searchdynamics" action="${pageContext.request.contextPath }/dynamic">
                <input type="text" name="search" value="${requestScope.searchString }">
                <input type="hidden" name="action" value="search">
                    <button id="submit" ><img  src="${pageContext.request.contextPath }/images/search.png" alt=""></button>
                </form>
                <a href="${pageContext.request.contextPath }/views/dynamic-release.jsp" class="a-launch">发布动态 </a>
            </div>
            <div class="clearfloat"></div>
        </div>
        <div class="clearfloat"></div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script>
    	
    </script>
</body>

</html>