<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>招聘信息详情展示</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <style>
        /* 初始设置：1.清除内外边距 2.去除li前面的小点 3.去除链接的下划线 */
        
        * {
            margin: 0;
            padding: 0;
        }
        
        li {
            list-style: none;
        }
        
        a {
            text-decoration: none;
        }
        /* 清除浮动 */
        
        .clearfix::after {
            content: " ";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }
        
        .clearfix {
            /*IE6、7专有*/
            *zoom: 1;
        }
       
        /* 招聘信息展示 */
        
        .recruitment {
            width: 1200px;
            margin: 0 auto;
            margin-top: 10px;
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
            position:relative;
        }
        .recruitment .alert {
            position: absolute;
            top: calc(41%);
            left: calc(16%);
        }
        .recruitment .re-left {
            background-color: whitesmoke;
            width: 800px;
        }
        
        .recruitment .re-left .jubao a {
            float: right;
            margin-right: 5px;
        }
        
        .recruitment .re-left .header {
            padding-top: 36px;
            height: aut0;
            border-bottom: 1px solid black;
            border-radius: 5px;
            margin-left: 38px;
            margin-right: 38px;
        }
        
        .recruitment .re-left .header h3 {
            float: left;
            font-size: 35px;
        }
        
        .recruitment .re-left .header h5 {
            float: right;
            margin-right: 10px;
            font-size: 22px;
            color: #6bbdd3;
        }
        .recruitment .re-left .header ul{
       		 margin-top:10px;
        }
        .recruitment .re-left .header ul li {
            float: left;
            margin-left: 16px;
            font-size: 17px;
            color: #4992c6;
            background-color: #f6fcff;
            border-radius: 5px;
            margin-bottom: 10px;
        }
        
        .recruitment .re-left .header .hot-words {
            font-size: 25px;
            height: 60px;
            text-align: left;
           
            color: #2e343b;
        }
        
        .recruitment .re-left .header h5 .Scalary {
            font-size: 35px;
        }
        
        .recruitment .re-left .details {
            margin-top: 20px;
           
            margin-right: 38px;
        }
        
        .recruitment .re-left .delivery {
            background-color: #6bbdd3;
            font-size: 27px;
            width: 350px;
            height: 65px;
            line-height: 65px;
            text-align: center;
            float: left;
            margin-top: 20px;
            margin-left: 38px;
            margin-bottom: 10px;
        }
        
        .recruitment .re-left .delivery a {
            color: #ffff;
        }
        
        .recruitment .re-left .consult {
            float: left;
            margin-top: 20px;
            margin-left: 5px;
        }
        
        .recruitment .re-left .consult a {
            color: #252525;
            font-size: 12px;
            width: 32px;
            /* display: block; */
        }
        
        .recruitment .re-left .consult img {
            display: block;
            margin: 0 auto;
            margin-top: 20px;
        }
        /* 右侧· */
        
        .recruitment .re-right {
            /* background-color: rebeccapurple; */
            width: 390px;
            float: right;
            margin-left: 10px;
        }
        
        .recruitment .re-right .header {
            text-align: center;
            height: 76px;
            line-height: 76px;
            font-size: 15px;
            color: #e4ba8e;
            background-color: #fcf5e8;
        }
        
        .recruitment .re-right .enterprise {
            /* margin-top: 38px; */
            padding-top: 22px;
            /* background-color: #4992c6; */
        }
        
        .recruitment .re-right .enterprise span,
        .check {
            display: block;
            margin-top: 16px;
            margin-left: 38px;
            color: #979a9e;
            font-size: 16px;
            /* height: 80px;
            line-height: 80px; */
        }
        
        .recruitment .re-right .enterprise .enter-name {
            color: #252525;
            font-size: 19px;
            font-weight: 700;
        }
        
        .identification {
            height: 35px;
            /* background-color: #fff; */
            line-height: 35px;
        }
        
        .identification .ident {
            display: inline;
            color: #ffc981;
            background-color: #fff7f1;
            border-radius: 5px;
            margin-right: 5px;
            padding-left: 2px;
        }
        
        .identification .tianYanCha {
            font-size: 12px;
            color: #252525;
        }
        
        .check {
            height: 64px;
            width: 320px;
            line-height: 64px;
            text-align: center;
            font-size: 20px;
            color:#6bbdd3;
            border: 1px solid #6bbdd3;
        }
        .details{
       		 border: none;
       		 background-color:#fff;
       		 font-size: 16px;
       		 color:black;
       		 margin: 8px auto;
       		 resize: none;
        }
    </style>
</head>

<body>
    <!-- 最上端的头部 -->
    <%@ include file="hander.jsp" %>
    <div class="recruitment">
      <!-- 警示框 -->
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong><a href="${pageContext.request.contextPath }/student?action=resume&SId=${sessionScope.student.getSId()}" target="_blank">建立简历</a></strong> 你还没有建立你的简历哦！
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <!-- 左侧 -->
        <div class="re-left">
            <div class="header clearfix">
                <div class="clearfix">
                    <h3>${sessionScope.redisplay.getJob().getJname() }</h3>
                    <h5><span class="Scalary">${sessionScope.redisplay.getSalary()}</span>元/月</h5>
                </div>
                <div class="hot-words">${sessionScope.redisplay.getRTitle()}</div>
                <ul>
                    <li>${sessionScope.redisplay.getDegree()}</li>
                    <li>${sessionScope.redisplay.getAddress()}</li>
                    <li>招聘人数:${sessionScope.redisplay.getRCount()}</li>
                </ul>
            </div>
            <textarea rows="10" cols="90" disabled="disabled" type="text" class="form-control pull-left details" id="discription" >${sessionScope.redisplay.getRContent()}</textarea>
            <div class="delivery">
            <c:choose>
            <c:when test="${sessionScope.enterprise !=null }">
    			<a href="javascript:;">仅可查看</a>
			</c:when>
			  <c:when test="${sessionScope.student ==null }">
    			<a href="javascript:;">仅可查看</a>
			</c:when>
             <c:when test="${requestScope.deliveryFlag!=null}">
            	<a href="javascript:;">已成功投递</a>
             </c:when>
            <c:when  test="${requestScope.success!=null}">
           <%--   <%System.out.println(request.getAttribute("exist")); %>--%>
             <%-- <% System.out.println(request.getAttribute("success"));%>  --%>
            	<a href="javascript:;">${requestScope.success}</a>
            </c:when>
            <c:otherwise>
            	<a href="${pageContext.request.contextPath }/delivery?action=delivery&SId=${sessionScope.student.getSId()}&RId=${sessionScope.redisplay.getRId()}">投递简历</a>
            </c:otherwise>
            </c:choose>
            </div>
          <%-- ${sessionScope.student.getSId()} --%>
        </div>
        <!-- 右侧 -->
        <div class="re-right">
            <div class="header">
                <span class="line">——</span>
                <span class="mingQi">名企</span>
                <span class="line">——</span>
            </div>
            <div class="enterprise">
                <span class="enter-name">${sessionScope.redisplay.getEnterprise().getEName()}</span>
                <span>工商执照号：${sessionScope.redisplay.getEnterprise().getEId()}</span>
                <span>负责人：${sessionScope.redisplay.getEnterprise().getEPrinName()}（${sessionScope.redisplay.getEnterprise().getEPosition()}）</span>
                <span class="identification"><div class="ident">企业招聘（已认证）</div><a href="https://www.tianyancha.com/search?key=&sessionNo=1669515131.63260936" class="tianYanCha" target="_blank">本信息由天眼查提供</a></span>
                <a href="#" class="check">查看其他招聘信息</a>
            </div>
        </div>
    </div>
    <!--表单  -->
	<form action="${pageContext.request.contextPath }/delivery" method="get" class="all">
		<input type="hidden" name="action" value="check">
		<input type="hidden" name="SId" value="${sessionScope.student.getSId()}">
		<input type="hidden" name="RId" value="${sessionScope.redisplay.getRId()}"s>
<%-- 		${sessionScope.redisplay.getRId()}"
 --%>	</form>    
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>
     <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script type="text/javascript">
   		  if(<%=request.getAttribute("exist")==null%>&&<%=request.getAttribute("resume_is_null")==null%>){
    		$(".all").submit();
    	} 
   		 $(".alert").hide();
   	  if(<%=request.getAttribute("resume_is_null")!=null%>){
   		 $(".alert").show();
   		 
	} 
	    $('textarea').each(function () {
	        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
	    }).on('input', function () {
	        this.style.height = 'auto';
	        this.style.height = (this.scrollHeight) + 'px';
	    });
    </script>
</body>

</html>