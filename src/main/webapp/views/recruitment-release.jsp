<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>新增招聘信息</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/dynamic-release.css">
</head>

<body>
    <!-- 最上端的头部 -->
    	<%@ include file="hander.jsp" %>
    <!-- 发布动态 -->
    <p class="re-p"><a href="./dynamic-list.html">>招聘管理</a></p>
    <div id="dynamic">
        <div class="release">发布招聘信息 <span class="notion">(*：必填项)</span></div>
        <form method="post" action="${pageContext.request.contextPath }/recruitment?action=insert&EId=${sessionScope.enterprise.getEId() }">
            <!-- *：必填项 -->
            <div class="body">
                <div class="Dtitle">
                    <label for="cb1"><span>*</span>招聘标题：</label>
                    <input type="text" name="title" id="cb1" placeholder="如：中国石化润滑油有限公司华东分公司2023年校园招聘">
                </div>
                <!-- 招聘学历要求 -->
                <div class="Rdegree">
                    <label for="Rdegree"><span>*</span>学历要求：</label>
                    <input type="text" id="Rdegree" name="degree" placeholder="如：专科及以上">
                </div>
                <!-- 招聘职位 -->
                <div class="Rjob">
                    <label for="Rjob">&nbsp; 招聘职位：</label>
                    <input type="text" name="job" id="Rjob" placeholder="如：会计">
                    <span class="notion">（校招时可直接填写“校招”）</span>
                </div>
                <!-- 招聘人数 -->
                <div class="Rcount">
                    <label for="Rcount"><span>*</span>招收人数：</label>
                    <input type="text" name="count" id="Rcount" placeholder="如：21">人
                </div>
                <!-- 招聘薪资 -->
                <div class="Rsalary"><label for="Rsalary"><span>*</span>薪资待遇：</label>
                    <input type="text" name="sa" id="Rsalary" placeholder="如：3000">元- <input type="text" name="lary" placeholder="如：15000">元</div>
                <!-- 工作地址 -->
                <div class="Raddress">
                    <label for="Raddress"><span>*</span>工作地址：</label>
                    <input type="text" name="address" id="Raddress" placeholder="如：上海市浦东新区">
                </div>
                <div class="Dcontent">
                    <label for="cb2"><span>*</span>职位详情：</label><br>
                    <textarea name="content" id="cb2" cols="30" rows="10" type="text" class="form-control pull-left" id="discription"></textarea>
                </div>
        </div>
        <div class="but">
            <input type="submit" value="提交">
<!--             <input type="button" value="保存">
 -->            <!-- formaction="dynamic-list.html":把表单提交至该页面 -->
            <!-- <input type="button" value="取消" formaction="dynamic-list.html"> -->
            <a href="${pageContext.request.contextPath }/views/recruitment-managing.jsp"><input type="button" value="取消" >
            </a><span class="notion">（一经发布，只可改动职位详情！）</span>
        </div>
        </form>
        <div class="clearfloat"></div>
        </div>
 
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script type="text/javascript">
	    $('textarea').each(function () {
	        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
	    }).on('input', function () {
	        this.style.height = 'auto';
	        this.style.height = (this.scrollHeight) + 'px';
	    });
    </script>
</body>

</html>