<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>大学生就业服务平台</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
     <link rel="stylesheet" href="${pageContext.request.contextPath }/css/enterprise-index.css">
    <style type="text/css">
    #hots ul {
	    display: flex;
	    flex-direction: row;
	    flex-wrap: wrap;
	    justify-content: start;
	}
	.information{
		margin-right:4px!important;
	}
	.new ul{
		padding: 0;
	}
	.new li{
		display: flex;
		justify-content:space-between;
		align-items: center;
	}
	.new li a{
		color:#6a6a6a;
	}
	.outer img{
		object-fit: cover;
	}
</style>
</head>

<body >
    <!-- 最上端的头部 -->
	<%@ include file="hander.jsp" %>
    <!-- 轮播图 -->
      <div class="outer">
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active" data-bs-interval="2000">
                <img src=" ${pageContext.request.contextPath }/images/zheng-01.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5></h5>
                    <p></p>
                </div>
            </div>
            <div class="carousel-item" data-bs-interval="2000">
                <img src="${pageContext.request.contextPath }/images/zheng3.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">

                </div>
            </div>
            <div class="carousel-item" data-bs-interval="2000">
                <img src="${pageContext.request.contextPath }/images/zheng4.png" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5></h5>
                    <p></p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
    </div>
    </div>
    
    <!-- 热门招聘信息展示 -->
    <div id="hots">
        <div class="hot-0"><span class="hot-1">热门招聘讯息 </span><span class="hot-2">HOT INFORMATION</span></div>
        <ul class="clearfix">
        <c:forEach items="${sessionScope.HotRecruitments }" var="item" end="5">
            <li>
                <div class="information clearfix">
                    <a href="${pageContext.request.contextPath }/recruitment?action=obtaindetails&RId=${item.getRId()}"><img src="${pageContext.request.contextPath }/images/${item.getRpicture() }" alt="">
                        <div><span class="job">${item.getJob().getJname() }</span><span class="scalary">${item.getSalary()}</span></div>
                    </a>
                </div>
            </li>
            </c:forEach>
        </ul>
    </div>
    <!-- 新闻资讯 -->
    <div id="news">
        <div class="new-0"><span class="new-1">新闻动态 </span><span class="new-2">NEWS CONTER</span></div>
        <div class="new clearfix">
            <a href="javascript:;"><img src="${pageContext.request.contextPath}/images/不负韶华.jpg" alt=""></a>
            <ul>
            <c:forEach items="${sessionScope.newRecruitments }" var="item" end="4">
                <li><span class="chat">【最新讯息】</span> <div class="text-truncate" style="max-width: 230px;"> <a href="${pageContext.request.contextPath }/recruitment?action=obtaindetails&RId=${item.getRId()}">${item.getRTitle() }</a></div><span class="time">${item.getRTime() }</span></li>
            </c:forEach>
               
            </ul>
        </div>
        <div class="new clearfix">
            <a href="#"><img src="${pageContext.request.contextPath }/images/不负韶华.jpg" alt=""></a>
            <ul>
                <li><span class="chat">【资助政策】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【资助政策】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【资助政策】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【资助政策】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【资助政策】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
            </ul>
        </div>
        <div class="new last clearfix">
            <a href="#"><img src="${pageContext.request.contextPath }/images/不负韶华.jpg" alt=""></a>
            <ul>
             <c:forEach items="${sessionScope.fiveDynamics }" var="item">
                <li><span class="chat">【动态圈子】</span> <a href="${pageContext.request.contextPath }/dynamic?action=display&id=${item.getDId()}"> ${item.getDTitle() }<span class="time">${item.getDTime() }</span></a></li>
               </c:forEach>
                <!-- <li><span class="chat">【动态圈子】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【动态圈子】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【动态圈子】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li>
                <li><span class="chat">【动态圈子】</span> 迎|“十一”主题优惠即将开幕 <span class="time">2014-10-20</span></li> -->
            </ul>
        </div>
    </div>
<!--表单  -->
	<form action="${pageContext.request.contextPath }/all" method="get" class="all" ><input type="hidden" name="action" value="all"></form>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
	<script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>

    <script>
	     if (<%=session.getAttribute("fiveDynamics") %>==null) {
	    	 document.querySelector(".all").submit();
		} 
	
    </script>
</body>

</html>