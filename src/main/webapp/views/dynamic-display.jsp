<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>头部和尾部</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <style>
	    .container{
	    	margin-top: 30px!important;
	    	margin-bottom: 20px!important;
	    }
        .dynamicdisplay {
            margin-top: 30px;
            border: solid 1px #353535;
            margin-bottom: 20px;
            min-height: 600px;
        }
        
        .dynamicdisplay h2,
        .dynamicdisplay .timeandsource {
            text-align: center;
        }
        
        .dynamicdisplay h2 {
            font-weight: bold;
            margin-bottom: 18px;
            margin-top: 40px;
            line-height: 1.5;
        }
        
        .dynamicdisplay .timeandsource {
            margin-bottom: 5px;
            font-size: 14px;
            color: #666;
        }
        
        .dyhead {
            border-bottom: solid 1px #15bded;
        }
        
        .dybody textarea {
            background-color: #fff!important;
            border: 0;
            resize: none;
            margin-bottom: 10px;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <!-- 最上端的头部 -->
   <%@ include file="hander.jsp" %>
    <!-- 动态展示部分 -->
    <div class="dynamicdisplay container">
        <div class="dyhead">
            <h2>${sessionScope.dynamic.getDTitle() }</h2>
            <div class="timeandsource">${sessionScope.dynamic.getDTime() } &nbsp;&nbsp; 来源：${sessionScope.dynamic.getStudent().getSNickname() }</div>
        </div>
        <div class="dybody"> <textarea rows="20" cols="90" disabled="disabled" type="text" class="form-control pull-left details" id="discription">${sessionScope.dynamic.getDContent()}</textarea></div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script>
        $('textarea').each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });
    </script>
</body>

</html>