<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>学生信息修改界面</title>
    <link rel="stylesheet" href="../CSS/updateStudent.css">
</head>

<body>
    <div class="top">
        <div class="top_box">
            <div class="top_left">
                <h1>大学生就业服务平台</h1>
            </div>
            <div class="top_right">
                <img src="../images/帐号.png" alt="">
                <h4>帐号XXX</h4>
                <div class="top_right_end"></div>
                <div class="top_droplist">
                    <a href="#">帐号管理</a>
                    <a href="#">退出登录</a>
                </div>
            </div>
        </div>
    </div>
    <div class="left">
        <div class="left_box">
            <div class="left_box_top">
                <img src="../images/帐号.png" alt="">
                <h3>帐号</h3>
            </div>
            <div class="left_box_a">
                <a href="#">学生信息管理</a>
                <a href="#">企业信息管理</a>
                <a href="#">招聘信息管理</a>
                <a href="#">就业政策管理</a>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="right_box">
            <div class="right_box_title">
                <span>&lt;返回</span>
                <h1>学生信息修改</h1>
                <hr>
            </div>
            <div class="right_box_content">
                <form action="">
                    <div class="content_row">
                        <label for="">学生姓名：</label>
                        <span>
                            <input type="text">
                        </span>
                    </div>
                    <div class="content_row">
                        <label for="">学生学号：</label>
                        <span>
                            <input type="text">
                        </span>
                    </div>
                    <div class="content_row">
                        <label for="">&emsp;手机号：</label>
                        <span>
                            <input type="tel">
                        </span>
                    </div>
                    <div class="content_row">
                        <label for="">&emsp;&emsp;学校：</label>
                        <span>
                            <input type="text">
                        </span>
                    </div>
                    <div class="content_row">
                        <label for="">&emsp;&emsp;专业：</label>
                        <span>
                            <input type="text">
                        </span>
                    </div>
                    <div class="content_row">
                        <label for="">&emsp;&emsp;状态：</label>
                        <span>
                            <select name="" id="">
                                <option value="">正常</option>
                                <option value="">封禁</option>
                                <option value="">留级</option>
                                <option value="">退学</option>
                            </select>
                        </span>
                    </div>
                    <div class="content_row">
                        <input type="submit" value="保存">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>