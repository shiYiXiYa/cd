<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/vsstyle.css"> -->
    <title>登录界面</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/login.css">
    <style type="text/css">
    .orange {
   		 background-color: rgb(243, 232, 77);
	}
</style>
</head>

<body>
    <div class="main">
        <!-- 申请框 注册-->
        <form id="apply-box" action="${pageContext.request.contextPath }/enterprise" method="post">
        <input type="hidden" name="action" value="register">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">工商注册号：</label>
                <input type="text" class="form-control" id="exampleInputEmail1"   aria-describedby="emailHelp" name="EId">
                <div id="emailHelp" class="form-text">这将是你的账号。</div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">公司名称</label>
                <input type="text" name="Ename" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">联系电话</label>
                <input type="text" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">提交</button>
            <div id="emailHelp" class="form-text .note">*一个工作日内将完成审查。</div>
        </form>
        
        <div class="opacity">
            <img src="${pageContext.request.contextPath }/images/login.jpg" alt="" class="login">
            <div id="box">  
                    <div class="select"><a href="#" class="orange stu">我是学生</a> <a href="#" class="enter">我是企业</a> </div> 
                    <!--企业登录  --> 
                <form  action="${pageContext.request.contextPath }/enterprise" method="post">
                    <input name="action" type="hidden" value="login">
                    <!-- 企业 -->
                    <div class="inputbox entre">
                    <label class="failure">${requestScope.enter_login_msg }</label><br>
                        <label>账号：</label><input type="text" placeholder="请输入工商注册号" name="eid" id="eid"><br> 密码：
                        <input type="password" name="Epwd" id="epwd"><br>
                        <span class="note">*初始密码：123456</span>
                        <button id="ebtn">登 录</button>
                        <div class="outer-apply">没有账号？
                            <span class="apply">点击申请</span>
                        </div>
                    </div>
                    </form>
                    <!-- 学生 登录-->
               <form action="${pageContext.request.contextPath }/student" method="post" id="stulogin">
                    <input name="action" type="hidden" value="login">
                    <div class="inputbox student">
                  		<label class="failure">${requestScope.student_login_msg }</label><br>
                        <label>学号：</label><input type="text" name="sid" id="sid"><br> <label>密码：</label> <input type="password" name="spwd" id="spwd"><br>
                        <button id="sbtn">登 &nbsp; 录</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <!-- <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script> -->
    <script>
    	 console.log( $(".select a"));
    	 if(<%=request.getAttribute("enter_login_msg")!=null%>){
    		$(".stu").toggleClass("orange"); 
    		$(".enter").toggleClass("orange"); 
    		 $(".entre").css("display", "block");
             $(".student").css("display", "none");
    	 } 
    	 console.log($(".select a"));
         $(".select a").on("click", function() {
             $(".select a").toggleClass("orange");
             if ($(this).index() === 0) {
                 $(".student").css("display", "block");
                 $(".entre").css("display", "none");
             } else {
                 $(".entre").css("display", "block");
                 $(".student").css("display", "none");
             }
         });

         $(".apply").on("click", function() {
             console.log('apply');
             $("#apply-box").show();
             $(".opacity").css("opacity", 0.2);
         });
         $('.btn-primary').on("click", function() {
             console.log("submit");
             if ($(".form-control").val().length == 0) {
                 alert('输入框不能为空');
                 return false;
             } else if ($(".form-control").eq(0).val().length != 13) {
                 alert('工商注册号必须为13位！');
                 return false;
             } else if ($(".form-control").eq(2).val().length != 11) {
                 alert('联系电话必须为11位！');
                 return false;
             } else
                 alert("信息已提交！");
             $("#apply-box").hide();
             $(".opacity").css("opacity", 1);
         });
         $("#sbtn").on("click", function() {
             if ($("#sid").val() == "" || $("#spwd").val() == "") {
                 alert('输入框不能为空');
                 return false;
             }
         });
         $("#ebtn").on("click", function() {
             if ($("#eid").val() == "" || $("#epwd").val() == "") {
                 alert('输入框不能为空');
                 return false;
             }
         });
       
    </script>
</body>


</html>