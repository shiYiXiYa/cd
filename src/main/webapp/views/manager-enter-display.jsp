<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>企业详情</title>
    <link rel="stylesheet" href="../CSS/companyDetails.css">
</head>
<body>
    <div class="top">
        <div class="top_box">
            <div class="top_left">
                <h1>大学生就业服务平台</h1>
            </div>
            <div class="top_right">
                <img src="../images/帐号.png" alt="">
                <h4>帐号XXX</h4>
                <div class="top_right_end"></div>
                <div class="top_droplist">
                    <a href="#">帐号管理</a>
                    <a href="#">退出登录</a>
                </div>
            </div>
        </div>
    </div>
    <div class="left">
        <div class="left_box">
            <div class="left_box_top">
                <img src="../images/帐号.png" alt="">
                <h3>帐号</h3>
            </div>
            <div class="left_box_a">
                <a href="#">学生信息管理</a>
                <a href="#">企业信息管理</a>
                <a href="#">招聘信息管理</a>
                <a href="#">就业政策管理</a>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="right_box">
            <div class="right_box_title">
                <span>&lt;返回</span>
                <h1>企业详情</h1>
                <hr>
            </div>
            <div class="right_box_content">
                <form action="">
                    <div class="content_row">
                        <label for="">企业号：</label>
                        <span>123456789</span>
                    </div>
                    <div class="content_row">
                        <label for="">企业名称：</label>
                        <span>当当网</span>
                    </div>
                    <div class="content_row">
                        <label for="">企业负责人：</label>
                        <span>张三</span>
                    </div>
                    <div class="content_row">
                        <label for="">负责人职位：</label>
                        <span>人事经理</span>
                    </div>
                    <div class="content_row">
                        <label for="">企业状态：</label>
                        <span>
                            <select name="" id="">
                                <option value="">正常</option>
                                <option value="">封禁</option>
                                <option value="">离开</option>
                            </select>
                        </span>
                    </div>
                    <div class="content_textarea">
                        <div><label for="">企业简介</label></div>
                        <p>1234567手机微信腾讯阿里巴巴</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>