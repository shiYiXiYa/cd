<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>新增招聘信息</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/dynamic-release.css">
</head>

<body>
    <!-- 最上端的头部 -->
   	<%@ include file="hander.jsp" %>
    <!-- 发布招聘信息 -->
    <p class="re-p"><a href="./dynamic-list.html">招聘信息管理</a>\修改招聘信息</p>
    <div id="dynamic">

        <div class="release">修改招聘信息 <span class="notion">(*：必填项)</span></div>
        <form action="${pageContext.request.contextPath }/recruitment">
            <!-- *：必填项 -->
            <div class="body">
                <div class="Dtitle">
                    <label for="cb1"><span>*</span>招聘标题：</label>
                    <input type="text" id="cb1" disabled value="${sessionScope.recruitment.getRTitle() }" >
                </div>
                <!-- 招聘学历要求 -->
                <div class="Rdegree">
                    <label for="Rdegree"><span>*</span>学历要求：</label>
                    <input type="text" id="Rdegree" disabled value="${sessionScope.recruitment.getDegree() }">
                </div>
                <!-- 招聘职位 -->
                <div class="Rjob">
                    <label for="Rjob">&nbsp; 招聘职位：</label>
                    <input type="text" name="" id="Rjob" disabled value="${sessionScope.recruitment.getJob().getJname() }">
                </div>
                <!-- 招聘人数 -->
                <div class="Rcount">
                    <label for="Rcount"><span>*</span>招收人数：</label>
                    <input type="text" name="" id="Rcount" disabled value="${sessionScope.recruitment.getRCount() }">人
                </div>
                <!-- 招聘薪资 -->
                <div class="Rsalary"><label for="Rsalary"><span>*</span>薪资待遇：</label>
                    <input type="text" name="" id="Rsalary" value="${sessionScope.recruitment.getSalary() }" disabled >元</div>

                <!-- 工作地址 -->
                <div class="Raddress">
                    <label for="Raddress"><span>*</span>工作地址：</label>
                    <input type="text" name=""  disabled id="Raddress" value="${sessionScope.recruitment.getAddress() }">
                </div>
                <div class="Dcontent">
                    <label for="cb2"><span>*</span>职位详情：</label><br>
                    <textarea  id="cb2" cols="30" rows="10" type="text" class="form-control pull-left" id="discription" >${sessionScope.recruitment.getRContent() }</textarea>
                </div>
                </div>
        </form>
       
        <div class="but">
            <input type="submit" value="提交">
            <a href="${pageContext.request.contextPath }/views/recruitment-managing.jsp"><input type="button" value="取消" f>
            </a>
        </div>
        <div class="clearfloat"></div>
        </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script type="text/javascript">
	    $('textarea').each(function () {
	        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
	    }).on('input', function () {
	        this.style.height = 'auto';
	        this.style.height = (this.scrollHeight) + 'px';
	    });
    </script>
</body>

</html>