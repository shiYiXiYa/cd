<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人信息</title>
    <link rel="stylesheet" href="../css/information.css">
</head>

<body>
    <div class="container flgure">
        <div class="content">
            <ul class="clearfix info">
                <li class="active">基础信息</li>
            </ul>
            <div class="basic">
                <div class="select active">
                    <p>
                        <span>编辑</span>
                        <span style="margin-left: 20px">保存</span>
                    </p>
                    <div class="left fl">
                        <div>
                            <label for="">企业号：</label><br>
                            <input type="text" value="3202002105308" class="id" disabled=“disabled”>
                        </div>
                        <div>
                            <label for="">企业名：</label><br>
                            <input type="text" value="宜宾伊瑞达紧固件有限公司" class="id" disabled=“disabled”>
                        </div>
                        <div>
                            <label for="">公司地址：</label><br>
                            <textarea type="text" rows="1" cols="50" class="id"> 中国上海市xxx区xxx路12号moumou大厦5层5xx室 </textarea>
                        </div>
                        <div>
                            <label for="">负责人：</label><br>
                            <input type="text" value="xx" class="id">
                        </div>
                        <div>
                            <label for="">职位：</label><br>
                            <input type="text" value="hr" class="id">
                        </div>
                        <div>
                            <label for="">企业简介：</label><br>
                            <textarea type="text" rows="0" cols="80" class="id">公司主要经营射钉紧固器材及其零配件、塑料制品、工模具、化工机械及其零配件及货物进出口业务。</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>