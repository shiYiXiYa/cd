<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>头部和尾部</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.0/font/bootstrap-icons.css">
    <style>
        * {
            font-family: "微软雅黑"
        }
        
        textarea {
            resize: none;
            background-color: #fff!important;
            border: none!important;
        }
        
        .box .return {
            float: left;
            text-align: center;
            text-decoration: none;
            padding-top: 10px;
            font-size: 30px;
            color: beige;
        }
        
        h3 {
            text-align: center;
            background-color: skyblue;
            font-size: 50px;
            font-weight: 700;
        }
        
        .resumedisplay {
            margin-top: 10px;
        }
        
        .main {
            margin-top: 20px;
            margin-bottom: 30px;
            min-height: 900px;
            border: #15bded 1px solid;
        }
        
        .part {
            display: flex;
            justify-content: space-between;
        }
        
        .part2 {
            display: flex;
            flex-direction: column;
        }
        
        .part,
        .part2 {
            min-height: 160px;
        }
        
        .part:nth-child(1) {
            margin: 0 30px;
        }
        
        .part .left1,
        .part .center1 {
            margin-top: 30px;
        }
        
        .part .center1 ul li {
            margin-bottom: 10px;
        }
        
        .part .center1 ul li i {
            margin-right: 5px;
        }
        
        .part .name {
            font-weight: 700;
            font-size: 26px;
            margin-bottom: 60px;
        }
        
        .part .job {
            font-weight: 700;
        }
        
        .part img {
            width: 180px;
            height: 180px;
            border-radius: 90px;
            object-fit: cover;
            border: 1px solid #15bded;
            margin-top: 5px;
        }
        
        .title {
            font-weight: 700;
            font-size: 20px;
            width: 100%;
            border-bottom: 1px #15bded dashed;
            margin-bottom: 8px;
            padding-bottom: 4px;
        }
        
        .title i {
            background-color: aqua;
            padding: 5px 7px;
            border-radius: 17px;
        }
        
        .partcontent {
            margin-bottom: 5px;
        }
        
        .left2,
        .right2 {
            min-width: calc(45%);
        }
        
        .rowschool ul {
            display: flex;
            margin-bottom: 0;
            font-weight: 700;
            justify-content: space-around;
        }
    </style>
</head>

<body>
    <!-- 个人简历展示部分 -->
    <div class="box">
        <div class="container">

            <h3>个人简历</h3>
        </div>
        <div class="resumedisplay container">
            <!-- 姓名、求职目标、年龄、电话、邮箱 -->
            <div class="container main">
                <div class="part">
                    <div class="left1">
                        <div class="name">${sessionScope.resume.getStudent().getSName() }</div>
                        <div class="job">求职目标：${sessionScope.resume.getJname() }</div>
                    </div>
                    <div class="center1">
                        <ul>
                            <li><i class="bi bi-house"></i>现居：${sessionScope.resume.getRAddress() }</li>
                            <li><i class="bi bi-calendar-date"></i>生日：${sessionScope.resume.getRBirth() }</li>
                            <li><i class="bi bi-telephone"></i>电话：${sessionScope.resume.getRsphone() }</li>
                            <li><i class="bi bi-envelope"></i>邮箱：${sessionScope.resume.getREmail() }</li>
                        </ul>
                    </div>
                    <div class="right1"><img src="/images/${sessionScope.resume.getRsImg() }" alt=""></div>
                </div>
                <!-- 毕业院校：专业、学历、主修课程 -->
                <div class="part2">
                    <div class="title"><i class="bi bi-mortarboard-fill"></i>毕业院校</div>
                    <div class="partcontent">
                        <div class="rowschool">
                            <ul>
                                <li>${sessionScope.resume.getRStudyTime() }</li>
                                <li>${sessionScope.resume.getStudent().getSSc() }</li>
                                <li>${sessionScope.resume.getStudent().getDName() }</li>
                                <li>${sessionScope.resume.getREducation() }</li>
                            </ul>
                        </div>
                        <textarea disabled="disabled" type="text" class="form-control pull-left details" id="discription">主修课程：${sessionScope.resume.getRLesson() }</textarea>
                    </div>
                </div>
                <!-- 实习经历： -->
                <div class="part2">
                    <div class="title"><i class="bi bi-people-fill"></i>实习经历</div>
                    <div class="partcontent"><textarea rows="" cols="" disabled="disabled" type="text" class="form-control pull-left details" id="discription">${sessionScope.resume.getRsexper() }</textarea>                        </div>
                </div>
                <!-- 荣誉奖励&技能证书 -->
                <div class="part">
                    <div class="left2">
                        <div class="title"><i class="bi bi-award-fill"></i>荣誉奖励</div>
                        <div class="partcontent"> <textarea rows="" cols="" disabled="disabled" type="text" class="form-control pull-left details" id="discription">${sessionScope.resume.getRHonor() }</textarea>
                        </div>
                    </div>
                    <div class="right2">
                        <div class="title"><i class="bi bi-book-fill"></i>技能证书</div>
                        <div class="partcontent"><textarea rows="" cols="" disabled="disabled" type="text" class="form-control pull-left details" id="discription">${sessionScope.resume.getRCertificate() }</textarea>

                        </div>
                    </div>
                </div>
                <!-- 自我评价 -->
                <div class="part2">
                    <div class="title"><i class="bi bi-person-fill"></i>自我评价</div>
                    <div class="partcontent"><textarea rows="" cols="" disabled="disabled" type="text" class="form-control pull-left details" id="discription">${sessionScope.resume.getRevaluation() }</textarea>                        </div>
                </div>
            </div>
        </div>

        <script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
        <script>
            $('textarea').each(function() {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function() {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });
        </script>
</body>

</html>