<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>大学生就业服务平台</title>
      <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
     <link rel="stylesheet" href="${pageContext.request.contextPath }/css/enterprise-index.css">
</head>

<body>
    <!-- 最上端的头部 -->
    <div id="head">
        <div class="container clearfix">
            <span class="l1">欢迎来到大学生就业服务平台</span>
            <div class="r1">
                <a href="#"><img src="${pageContext.request.contextPath }/images/tel.png" alt="">400-00-000123</a>
                <a href="#"><img src="${pageContext.request.contextPath }/images/mail.png" alt="">3032324123@gmail.com</a>
            </div>
        </div>
    </div>
    <div id="nav-0">
        <div class="nav">
            <span class="offer">offer</span>
            <div class="r2">
                <ul class="clearfix">
                <!-- 测试 -->
                  <c:if test="${sessionScope.enterprise !=null }">
                     <li>
                        <a href="${pageContext.request.contextPath }/recruitment?action=manager&EId=${sessionScope.enterprise.getEId()}">招聘管理</a> 
                        
                    </li>
                     
                    <li> <a href="javascript:;"><img src="${pageContext.request.contextPath }/images/${sessionScope.enterprise.getEImg()} " alt=""> ${sessionScope.enterprise.getEName() }</a>
                        </li>
                        <li><a href="${pageContext.request.contextPath }/all?action=cancel">注销</a></li>
                   </c:if> 
                    <c:if test="${sessionScope.student !=null }">
                    	 <li>
                     		  <a href="${pageContext.request.contextPath }/student?action=resume&SId=${sessionScope.student.getSId()}" target="_blank">简历管理</a>
                   		 </li>
                   		 <li>
                        <a href="${pageContext.request.contextPath }/delivery?action=record&SId=${sessionScope.student.getSId()}" target="_blank">投递记录</a>
                    </li>
                    	<li>
                       		 <a href="${pageContext.request.contextPath }/views/dynamic-list.jsp">动态广场</a>
                   		 </li>
                   		<li> <a href="#"><img src="${pageContext.request.contextPath }/images/${sessionScope.student.getSImg()} " alt=""> ${sessionScope.student.getSName() }</a>
                        </li>
                        <li><a href="${pageContext.request.contextPath }/all?action=cancel">注销</a></li>
					</c:if>
					
					 <c:if test="${sessionScope.student ==null &&sessionScope.enterprise ==null}">
						  <li>
	                        <a href="${pageContext.request.contextPath }/views/dynamic-list.jsp">动态广场</a>
	                    </li>
                       <li> <a href="${pageContext.request.contextPath }/views/login.jsp">登录</a></li>
                        </c:if>
                </ul>
            </div>
        </div>
    </div>