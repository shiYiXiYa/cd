<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人简历展示（修改）</title>
    <style>
        h3 {
            text-align: center;
            font-size: 50px;
            font-weight: 700;
        }
        
        h4 {
            text-align: center;
            font-size: 18px;
            font-weight: normal;
            color: gray;
        }
        
        table {
            margin: 0 auto;
            border: 1px solid black;
            border-collapse: collapse;
        }
        
        table {
            width: 1200px;
        }
        
        textarea {
            resize: none;
        }
        
        td {
            width: 200px;
            height: 70px;
            border: 1px solid black;
            font-size: 25px;
            /* text-indent: 2em; */
            text-align: center;
            /* font-weight: 700; */
        }
        
        td input,
        td textarea {
            padding: 0px;
            width: 100%;
            height: 100%;
            font-size: 25px;
            display: block;
            border-width: 0px;
        }
        
        td input {
            outline: none;
        }
        
        td img {
            width: calc(100%);
            height: calc(300%);
            object-fit: cover;
        }
        
        .black {
            font-weight: 700;
        }
        
        .modification {
            display: flex;
            justify-content: center;
        }
        
        .modification input {
            width: 80px;
            height: 40px;
            margin-top: 10px;
        }
        
        .modification input:hover {
            background-color: skyblue;
            outline: none;
            border: none;
        }
    </style>
</head>

<body>
    <div class="box">
        <h3>个人简历</h3>
        <h4>(直接修改后点击保存可查看)</h4>
        <form action="${pageContext.request.contextPath }/resume" method="post">
        	<input type="hidden" name="action" value="update">
        	<input type="hidden" name="img" value="" id="myimg">
        	<input type="hidden" name="SId" value="${sessionScope.student.getSId() }">
            <table>
                <tr>
                    <td class="black">姓 名</td>
                    <td colspan="2">${sessionScope.findStudentObj.getSName() }</td>   
                    <td class="black">出生年月</td>
                    <td><input type="text" name="birthday"></td>
                    <td rowspan="3"><input type="file" name="picture" id="file"><img src="" alt="" id="image"></td>
                </tr>
                <tr>
                    <td class="black">求职目标</td>
                    <td colspan="1"><input type="text" name="Jname"></td>
					  <td class="black">邮箱</td>
                    <td colspan="2"><input type="text" name="email"></td>
                </tr>
                <tr>
                    <td class="black">所在学校</td>
                    <td colspan="2">${sessionScope.findStudentObj.getSSc() }</td>
                    <td class="black">学 历</td>
                    <td colspan="2"><input type="text" name="degree"></td>
                </tr>
                <tr>
                    <td class="black">所学专业</td>
                    <td colspan="2">${sessionScope.findStudentObj.getDName() }</td>
                    <td class="black">现居地</td>
                    <td colspan="3"><input type="text" name="address"></td>

                </tr>
                <tr>
                    <td class="black">就读时间</td>
                    <td colspan="3"><input type="text" name="studytime"></td>
                    <td class="black">联系电话</td>
                    <td colspan="2"><input type="text" name="phone"></td>
                </tr>
                <tr>
                    <td class="black">主修课程</td>
                    <td colspan="6"><textarea name="lesson" id="information" cols="30" rows="10" type="text" class="form-control pull-left details" id="discription"></textarea></td>

                </tr>
                <tr>
                    <td class="black">实习经历</td>
                    <td colspan="6"><textarea name="experience" id="experience" cols="30" rows="10" type="text" class="form-control pull-left details" id="discription"></textarea></td>

                </tr>
                <tr>
                    <td class="black">荣誉奖励</td>
                    <td colspan="6"><textarea name="honor" id="experience" cols="30" rows="10" type="text" class="form-control pull-left details" id="discription"></textarea></td>

                </tr>
                <tr>
                    <td class="black">技能证书</td>
                    <td colspan="6"><textarea name="certificate" id="experience" cols="30" rows="10" type="text" class="form-control pull-left details" id="discription"></textarea></td>

                </tr>
                <tr>
                    <td class="black">自我评价</td>
                    <td colspan="6"><textarea name="evaluation" id="experience" cols="30" rows="10" type="text" class="form-control pull-left details" id="discription"></textarea></td>
                </tr>

            </table>

            <div class="modification">
                <input type="submit" value="保   存" id="sub">
            </div>
        </form>
    </div>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
    <script>
        $("#image").hide();
        $("#file").on("change", function(e) {
            console.log("1");
            console.log(this.files[0].type);
            var path = e.currentTarget.files[0].name; //文件名
            console.log(path);
            $("#myimg").val(path);
            $("#image").show();
            $("#image").prop("src", "/images/" + path);
            $("#file").hide()
        });
        $("#image").on("click", function() {
            console.log("3");
            $("#file").click();
        });
       $("#sub").on("click",function(){
    	   if ($("#myimg").val() == "" || $("#myimg").val() == null) {
               alert("必须传入照片！");
               return false;
           }
           let length = $("input[type='text']").length;
           console.log(length);
           for (var i = 0; i < length; i++) {
               if ($("input[type='text']")[i].value == "") {
                   console.log(i);
                   alert("相关信息必须填完哦！");
                   return false;
               }
           }
           let l = $("textarea").length;
           for (var i = 0; i < l; i++) {
               var str = $("textarea").get(i).value;
               if (str == "") {
                   console.log(i);
                   alert("相关信息必须填完哦！");
                   return false;
               }
           }
       })
        $('textarea').each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });
    </script>
</body>

</html>