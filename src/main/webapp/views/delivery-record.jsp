<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>头部和尾部</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <style>
        .flexitem {
            display: flex;
            justify-content: space-between;
        }
       
    .tixing{
    	color:navy;
    	text-align: center;
    	font-size:20px;
    	margin-top:140px;
    	font-weight: 700;
    }
    
    </style>
   
</head>

<body>
    <!-- 最上端的头部 -->
	<%@ include file="hander.jsp" %>
    <!-- 学生的投递记录 -->
    <div class="container">
    <c:choose>
    <c:when test="${requestScope.record_is_null!=null }">
    	<div class="tixing">${requestScope.record_is_null }</div>
    </c:when>
    <c:otherwise>
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th scope="col">公司名称</th>
                    <th scope="col">招聘标题</th>
                    <th scope="col">招聘人数</th>
                    <th scope="col">录取状态</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${sessionScope.deliveryRecord}" var="item">
                <tr>
                    <td>${item.getRecruitment().getEnterprise().getEName()}</td>
                    <td>${item.getRecruitment().getRTitle()}</td>
                    <td>${item.getRecruitment().getRCount()}</td>
                    <td class="flexitem">
                    <c:choose>
                    <c:when test="${item.getAstate()==1 }">
                        <div class="state">已录取</div>
                      </c:when>
                      <c:otherwise><div class="state">待通知</div></c:otherwise>
                      </c:choose>
                        <div><a href="${pageContext.request.contextPath }/recruitment?action=obtaindetails&RId=${item.getRId()}" target="_blank" rel="noopener noreferrer">查看详情</a></div>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
        </c:otherwise>
        </c:choose>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="${pageContext.request.contextPath }/images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
    <script src="${pageContext.request.contextPath }/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath }/js/jQuery.js"></script>
</body>

</html>