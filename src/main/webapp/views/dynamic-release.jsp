<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>发布动态</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/header&footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/dynamic-release.css">
</head>

<body>
    <!-- 最上端的头部 -->
   <%@ include file="hander.jsp" %>
    <!-- 发布动态 -->
    <p class="re-p"><a href="./dynamic-list.html">动态广场</a>\发布详情</p>
    <div id="dynamic">

        <div class="release">发布动态</div>
        <form method="post" action="${pageContext.request.contextPath }/dynamic?action=release&SId=${sessionScope.student.getSId()}">
<%--          <form method="post" action="${pageContext.request.contextPath }/dynamic?action=release&SId=20101020201">
 --%>            <!-- *：必填项 -->
            <div class="Dtitle">
                <label for="cb1"><span>*</span> 动态标题：</label>
                <input type="text" name="title" id="cb1">
            </div>
            <div class="Dcontent">
                <label for="cb2"><span>*</span>动态内容：</label><br>
                <textarea name="content" id="cb2" cols="30" rows="10"></textarea>
            </div>
        
        <div class="but">
            <input type="submit" value="提交">
           
           <!--  <input type="button" value="保存"> -->
            <!-- formaction="dynamic-list.html":把表单提交至该页面 -->
            <!-- <input type="button" value="取消" formaction="dynamic-list.html"> -->
            <a href="${pageContext.request.contextPath }/fenye?action=dynamic"><input type="button" value="取消" formaction="dynamic-list.jsp">
            </a>
        </div> 
        </form>
        <div class="clearfloat"></div>

    </div>
    <!-- footer -->
    <div class="footer">
        <div class="content">
            <p class="c1"><a href="#">网站介绍</a> | <a href="#">联系我们</a></p>
            <p class="c3">京ICP备19004913号-1 <img src="../images/police.png" alt="">京公网安备11010202009747号</p>
        </div>
    </div>
</body>

</html>