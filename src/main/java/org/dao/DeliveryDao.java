package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.model.Delivery;
import org.model.Student;
import org.utils.DButils;

public class DeliveryDao {
	public List<Delivery> findDelivery(Integer RId) throws SQLException{
		String sql="select * from delivery where RId="+RId;
		System.out.println("查找投递简历对象列表的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//statement.setString(1, SId);
		//查询
		ResultSet rs = statement.executeQuery();
		List<Delivery> items=new ArrayList<>();
		Delivery delivery=null;
		if(rs.next()) {
			delivery=new Delivery();
			delivery.setSId(rs.getString("SId"));
			delivery.setAstate(rs.getInt("Astate"));	
			delivery.setRId(rs.getInt("RId"));
			items.add(delivery);
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**
	 * 根据SId查找相关投递记录
	 * @param RId
	 * @return
	 * @throws SQLException
	 */
	public List<Delivery> findDelivery(String sId) throws SQLException{
		String sql="select * from delivery where SId="+sId;
		System.out.println("查找投递简历对象列表的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//statement.setString(1, SId);
		//查询
		ResultSet rs = statement.executeQuery();
		List<Delivery> items=new ArrayList<>();
		Delivery delivery=null;
		while(rs.next()) {
			delivery=new Delivery();
			delivery.setSId(rs.getString("SId"));
			delivery.setAstate(rs.getInt("Astate"));	
			delivery.setRId(rs.getInt("RId"));
			items.add(delivery);
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**
	 * 根据招聘信息SId/RId来查找相关投递消息Delivery
	 * @param RId
	 * @return
	 * @throws SQLException 
	 */
	public Delivery findDelivery(Integer RId,String SId) throws SQLException {
		String sql="select * from delivery where RId="+RId+" and SId='"+SId+"'";
		System.out.println("查找投递简历对象1的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//statement.setString(1, SId);
		//查询
		ResultSet rs = statement.executeQuery();
		Delivery delivery=null;
		if(rs.next()) {
			delivery=new Delivery();
			delivery.setSId(rs.getString("SId"));
			delivery.setAstate(rs.getInt("Astate"));	
			delivery.setRId(rs.getInt("RId"));
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return delivery;
	}
	/**
	 * 投递简历
	 * @param delivery
	 * @throws SQLException
	 */
	public void insertDelivery(Delivery delivery) throws SQLException {
		String sql="insert into delivery(SId,RId) values('"+delivery.getSId()+"',"+delivery.getRId()+")";
		System.out.println("投递简历的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		int rs=statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入成功！");
		else System.out.println("插入失败！");
		//关闭连接
		statement.close();
		connection.close();
	}
	/**
	 * 改变录取状态
	 * 管理员 1->0
	 * 企业 0->1(录取)
	 * @param stateInteger
	 * @throws SQLException
	 */
	public void changeAstate(Integer state,String SId,Integer RId) throws SQLException {
		if(state==1) state=0;
		else state=1;
		String sql="update delivery set Astate="+state+" where SId='"+SId+"' and RId="+RId;
		System.out.println("修改录取状态的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		int rs=statement.executeUpdate(sql);
		if(rs==1) System.out.println("修改成功！");
		else System.out.println("修改失败！");
		//关闭连接
		statement.close();
		connection.close();
	}
	
//test
	/**
	 * 通过一堆RId查找投递信息
	 * @param rIdlist
	 * @return
	 * @throws SQLException 
	 */
	public List<Delivery> findDeliverysByRidString(String rIdlist) throws SQLException {
		String sql="select * from delivery where RId in "+rIdlist;
		System.out.println("通过一堆RId查找投递信息找对象列表的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//statement.setString(1, SId);
		//查询
		ResultSet rs = statement.executeQuery();
		List<Delivery> items=new ArrayList<>();
		Delivery delivery=null;
		while(rs.next()) {
			delivery=new Delivery();
			delivery.setSId(rs.getString("SId"));
			delivery.setAstate(rs.getInt("Astate"));	
			delivery.setRId(rs.getInt("RId"));
			items.add(delivery);
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
}
