package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.model.Dynamic;
import org.model.Enterprise;
import org.utils.DButils;

public class EnterpriseDao {
	/**
	 * 根据EId查找得到企业对象
	 * @return
	 * @throws SQLException
	 */
	public Enterprise findByEnterpriseObj(String EId) throws SQLException {
		String sql="select * from enterprise where EId='"+EId+"'";
		System.out.println("根据id查询相应的enterprise 对象的sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		Enterprise enterprise = new Enterprise();
		if(rs.next()) {
			enterprise.setEId(EId);
			enterprise.setECur(rs.getString("ECur"));
			enterprise.setEImg(rs.getString("EImg"));
			enterprise.setEName(rs.getString("EName"));
			enterprise.setEPosition(rs.getString("EPosition"));
			enterprise.setEPrinName(rs.getString("EPrinName"));
			enterprise.setEPwd(rs.getString("EPwd"));
			enterprise.setEstate(rs.getInt("Estate"));
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return enterprise;
	}
	/**
	 * 更新企业头像
	 * @param s
	 * @throws SQLException 
	 */
	public void changeEnterpriseImg(Enterprise s) throws SQLException {
		String sql="update Enterprise set EImg='"+s.getEImg()+"' where EId='"+s.getEId()+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新企业头像成功！");
		else System.out.println("更新企业头像失败！");
		//关闭
		statement.close();
		connection.close();
	}
	/**
	 * 企业用户自己可修改自己的负责人姓名、职称、企业简介（ECur） flag=1
	 * 管理员修改企业信息 可修改其状态 					 flag=2
	 * 企业用户自己可修改自己的密码		 				 flag=3
	 * @param s
	 * @param flag
	 * @throws SQLException 
	 */
	public void changeEnterpriseInfo(Enterprise s,Integer flag) throws SQLException {
		String sql;
		if(flag==1) 
			sql="update enterprise set EPrinName='"+s.getEPrinName()+"',"+"ECur='"+s.getECur()+"EPosition='"+s.getEPosition()+"' where EId='"+s.getEId()+"'";
		else if(flag==2)
			sql="update enterprise set Estate='"+s.getEstate()+"' where EId='"+s.getEId()+"'";
		else {
			sql="update enterprise set EPwd='"+s.getEPwd()+"' where EId='"+s.getEId()+"'";
		}
		System.out.println("根据flag更新enterprise 对象的sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新成功！");
		else System.out.println("更新失败！");
		//关闭
		statement.close();
		connection.close();
	}
	
	/**
	 * 多条件查询企业用户(除状态)
	 * @return
	 * @throws SQLException
	 */
	public List<Enterprise> searchEnterprises(Enterprise enterprise) throws SQLException{
		String sql="select * from enterprise where EId='"+enterprise.getEId()+"' or EName like '%"+enterprise.getEName()+"%' or EPosition like '%"+enterprise.getEPosition()+"%'";
		System.out.println("根据多条件查询相应的enterprise列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Enterprise> items= new ArrayList<>();
		while(rs.next()) {
			Enterprise enterprise2 = new Enterprise();
			enterprise2.setEId(rs.getString("EId"));
			enterprise2.setECur(rs.getString("ECur"));
			enterprise2.setEImg(rs.getString("EImg"));
			enterprise2.setEName(rs.getString("EName"));
			enterprise2.setEPosition(rs.getString("EPosition"));
			enterprise2.setEPrinName(rs.getString("EPrinName"));
			enterprise2.setEstate(rs.getInt("Estate"));
			items.add(enterprise2);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	
	/**
	 * 根据状态查找企业
	 * @param s
	 * @return
	 * @throws SQLException
	 */
	public List<Enterprise> searchEnterprisesByEstste(Integer s) throws SQLException{
		String sql="select * from enterprise where Estste="+s;
		System.out.println("根据状态查询相应的enterprise列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Enterprise> items= new ArrayList<>();
		while(rs.next()) {
			Enterprise enterprise2 = new Enterprise();
			enterprise2.setEId(rs.getString("EId"));
			enterprise2.setECur(rs.getString("ECur"));
			enterprise2.setEImg(rs.getString("EImg"));
			enterprise2.setEName(rs.getString("EName"));
			enterprise2.setEPosition(rs.getString("EPosition"));
			enterprise2.setEPrinName(rs.getString("EPrinName"));
			enterprise2.setEstate(s);
			items.add(enterprise2);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	
	/**
	 * 添加企业用户
	 * 注册——>管理员审核
	 * @param enterprise
	 * @throws SQLException
	 */
	public void insertEnterprise(Enterprise enterprise) throws SQLException {
		String sql="insert into enterprise(EId,EName) values('"+enterprise.getEId()+"','"+enterprise.getEName()+"')";
		System.out.println("插入新企业的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入成功！");
		else System.out.println("插入失败！");
		//关闭
		statement.close();
		connection.close();
	}
}

