package org.dao;

import java.awt.event.ItemEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.model.Dynamic;
import org.model.Student;
import org.utils.DButils;



public class DynamicDao {
	/**
	 * 根据页码寻找相应的动态列表
	 * @param page1
	 * @param page2
	 * @return
	 * @throws SQLException
	 */
	public List<Dynamic> findDynamicsByPages(Integer page1,Integer page2) throws SQLException{
		String sql="select * from dynamic,student where dynamic.SId=student.SId order by DTime desc limit "+page1+","+page2;
		System.out.println("（分页）根据页码寻找相应的动态列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Dynamic> items= new ArrayList<>();
//		System.out.println("rs:"+rs);
		while(rs.next()) {
			Dynamic item = new Dynamic();
			Student s=new Student();
			s.setSNickname(rs.getString("SNickname"));
			s.setSImg(rs.getString("SImg"));
			item.setDContent(rs.getString("DContent"));
			item.setDId(rs.getInt("DId"));
			item.setDTime(rs.getString("DTime"));
			item.setDTitle(rs.getString("DTitle"));
			item.setSId(rs.getString("student.SId"));
			item.setStudent(s);
			items.add(item);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
}
	/**
	 * 计数动态列表数目，并按照时间先后顺序寻找
	 * @throws SQLException 
	 */
	public Integer getCount() throws SQLException {
		String sql="select * from dynamic,student where dynamic.SId=student.SId order by DTime ";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		Integer sumInteger=0;
		while(rs.next()) {
			sumInteger++;
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return sumInteger;
	}
	/**
	 * 根据多条件查询动态列表
	 * 查询条件：用户名、标题关键字（like）、内容关键字、、、、、、
	 * @param dynamic
	 * @return
	 * @throws SQLException 
	 */
	public List<Dynamic> searchDynamics(Dynamic dynamic) throws SQLException {
		StudentDao studentDao = new StudentDao();	
		List<Student> itemslist=studentDao.searchStudents(dynamic.getStudent());
		String string="";
		if (itemslist.size()!=0) {
			string+=" or SId in (";
			for(Student item:itemslist) {
				string=string+"'"+item.getSId()+"',";
			}
			string=string.substring(0, string.length()-1);
			string=string+")";
		}
		System.out.println(string);
		String sql="select * from dynamic where DTitle like '%"+dynamic.getDTitle()+"%' or DContent like '%"+dynamic.getDContent()+"%'"+string;
//		System.out.println("根据多条件查询相应的动态列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		System.out.println(sql);
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Dynamic> items= new ArrayList<>();
		while(rs.next()) {
			Dynamic item = new Dynamic();
			item.setDContent(rs.getString("DContent"));
			item.setDId(rs.getInt("DId"));
			item.setDTime(rs.getString("DTime"));
			item.setDTitle(rs.getString("DTitle"));
			item.setSId(rs.getString("SId"));
			items.add(item);
		}
		//判断列表是否为空
		if(items.isEmpty()) {
			System.out.println("empty");
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**	
	 * 用户发布新的动态
	 * @param dynamic
	 * @throws SQLException
	 */
	public void insertDynamic(Dynamic dynamic) throws SQLException {
		String sql="insert into dynamic (DContent,DTitle,SId) values('"+dynamic.getDContent()+"','"+dynamic.getDTitle()+"','"+dynamic.getSId()+"')";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入成功！");
		else System.out.println("插入失败！");
		//关闭
		statement.close();
		connection.close();
	}
	
//test
	public Dynamic finddynamicObj(Integer DId) throws SQLException {
		String sql="select * from dynamic where DId="+DId;
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		Dynamic dynamic=null;
		if(rs.next()) {
			dynamic=new Dynamic();
			dynamic.setDContent(rs.getString("DContent"));
			dynamic.setDId(rs.getInt("DId"));
			dynamic.setDTime(rs.getString("DTime"));
			dynamic.setDTitle(rs.getString("DTitle"));
			dynamic.setSId(rs.getString("SId"));
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return dynamic;
	}
}
