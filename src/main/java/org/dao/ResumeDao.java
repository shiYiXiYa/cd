package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.model.Job;
import org.model.Resume;
import org.utils.DButils;

public class ResumeDao {
	/**
	 * sid搜索得到简历对象
	 * @param RId
	 * @return
	 * @throws SQLException 
	 */
	public Resume findResumeObj(String sId) throws SQLException {
		String sql="select * from resume where SId='"+sId+"'";
		System.out.println("根据RId查询相应的简历对象sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		Resume resume = null;
		if(rs.next()) {
			resume = new Resume();
			resume.setRsId(rs.getInt("RsId"));
			resume.setRevaluation(rs.getString("Revaluation"));
			resume.setRStudyTime(rs.getString("RStudyTime"));
			resume.setRsphone(rs.getString("Rsphone"));
			resume.setRsexper(rs.getString("Rsexper"));
			resume.setRsImg(rs.getString("RsImg"));
			resume.setSId(rs.getString("SId"));
			resume.setJname(rs.getString("Jname"));
			resume.setRBirth(rs.getString("RBirth"));
			resume.setRAddress(rs.getString("RAddress"));
			resume.setREmail(rs.getString("REmail"));
			resume.setRCertificate(rs.getString("RCertificate"));
			resume.setRHonor(rs.getString("RHonor"));
			resume.setREducation(rs.getString("REducation"));
			resume.setRLesson(rs.getString("RLesson"));
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return resume;
	}
	/**
	 * 学生用户编辑修改个人简历
	 * 无insert功能
	 * 需要做修改,数据太过冗余
	 * @param resume
	 * @throws SQLException 
	 */
	public void changeResumeInfo(Resume resume) throws SQLException {
		String sql="update resume set Revaluation='"+resume.getRevaluation()
		+"', REducation='"+resume.getREducation()+"', Rsphone='"+resume.getRsphone()
		+"', Rsexper='"+resume.getRsexper()+"', Jname='"+resume.getJname()+"', RBirth='"+resume.getRBirth()
		+"', RStudyTime='"+resume.getRStudyTime()
		+"', RAddress='"+resume.getRAddress()+"', REmail='"+resume.getREmail()
		+"', RsImg='"+resume.getRsImg()+"', RLesson='"+resume.getRLesson()
		+"', RHonor='"+resume.getRHonor()+"', RCertificate='"+resume.getRCertificate()
		+"' where SId='"+resume.getSId()+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新学生头像成功！");
		else System.out.println("更新学生头像失败！");
		//关闭连接
		statement.close();
		connection.close();
	}
	
	
}
