package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.model.Job;
import org.model.Recruitment;
import org.utils.DButils;

public class RecruitmentDao {
	/**
	 * 根据ID查找招聘信息
	 * 如：点击事件，选中事件、、、、、、
	 * @param RId
	 * @return
	 * @throws SQLException 
	 */
	public Recruitment findRecruitmentObj(Integer RId) throws SQLException {
		String sql="select * from recruitment where RId="+RId;
		System.out.println("根据RId查询相应的招聘信息对象sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		Recruitment recruitment = new Recruitment();
		if(rs.next()) {
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
		}
//		System.err.println("RId"+recruitment.getRId());
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return recruitment;
	}
	/**
	 * 根据企业ID找招聘信息
	 * @param EId
	 * @return
	 * @throws SQLException
	 */
	public List<Recruitment> searchByEId(String EId) throws SQLException{
		String sql="select * from recruitment where EId='"+EId+"' order by Rstate";
		System.out.println("根据企业id查询相应的招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment=null;
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
		}
	/**
	 * 根据招聘的职位id搜索相关的招聘信息
	 * 需要根据输入的职位名先搜索到ID
	 * @param RName
	 * @return
	 * @throws SQLException 
	 */
	public List<Recruitment> searchByJname(Integer JId) throws SQLException{
		String sql="select * from recruitment where JId="+JId+" and Rstate=0";
		System.out.println("根据职位id查询相应的招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment;
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**
	 * 搜索最新的招聘信息
	 * @return
	 * @throws SQLException 
	 */
	public List<Recruitment> searchNewRecruitments() throws SQLException{
//		String sql="select * from recruitment where Rstate= 0 order by RTime desc limit 0,5";
		String sql=" select * from recruitment,job where Rstate= 0 and recruitment.JId=job.JId order by RTime desc limit 0,5;";
		System.out.println("根据时间查询相应的最新的六条招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment;
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
			Job job = new Job();
			job.setJId(rs.getInt("JId"));
			job.setJname(rs.getString("Jname"));
			recruitment.setJob(job);
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**
	 * 搜索最热的招聘信息
	 * @return
	 * @throws SQLException 
	 */
	public List<Recruitment> searchHotRecruitments() throws SQLException{
		// select * from recruitment where Rstate= 0 order by Rkick desc limit 0,6;取六条最热新闻
//		String sql="select * from recruitment where Rstate= 0 order by Rkick desc limit 0,6";
		String sql="select * from recruitment,job where Rstate= 0 and recruitment.JId=job.JId order by Rkick desc limit 0,6";
		System.out.println("根据点击数查询相应的最热的六条招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment;
//		JobDao jobDao = new JobDao();
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
//			recruitment.setJob(jobDao.findJobObj(rs.getInt("JId")));
			Job job = new Job();
			job.setJId(rs.getInt("JId"));
			job.setJname(rs.getString("Jname"));
			recruitment.setJob(job);
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
	/**
	 * 新增招聘信息
	 * @param recruitment
	 * @throws SQLException 
	 */
	public void insertRecruitment(Recruitment recruitment) throws SQLException {
		String sql="insert into recruitment(RContent,EId,JId,Salary,Degree,Address,RTitle,RCount) values('"+recruitment.getRContent()
		+"','"+recruitment.getEId()+"',"+recruitment.getJId()+",'"+recruitment.getSalary()
		+"','"+recruitment.getDegree()+"','"+recruitment.getAddress()+"','"+recruitment.getRTitle()+"',"+recruitment.getRCount()+")";
		System.out.println("新增加招聘信息的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//修改
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入招聘信息成功！");
		else System.out.println("插入招聘信息失败！");
//				关闭
		statement.close();
		connection.close();
	}
	/**
	 * 修改招聘信息
	 * RContent  RTitle RCount
	 * @param recruitment
	 * @throws SQLException 
	 */
	public void changeRecruitment(Recruitment recruitment) throws SQLException {
		String sql="update recruitment set RContent='"+recruitment.getRContent()+"',RTitle='"+recruitment.getRTitle()+"', RCount="+recruitment.getRCount()
		+" where EId="+recruitment.getEId();
		System.out.println("修改招聘信息的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//修改
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("修改招聘信息成功！");
		else System.out.println("修改招聘信息失败！");
//				关闭
		statement.close();
		connection.close();
	}
	/**
	 * 删除招聘信息0——>1
	 * @param Rstate
	 * @throws SQLException 
	 */
	public void changeRstate(Integer Rstate,Integer RId) throws SQLException {
		
		String sql="update recruitment set Rstate="+Rstate+" where RId="+RId;
		System.out.println("删除招聘信息的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//修改
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("删除招聘信息成功！");
		else System.out.println("删除招聘信息失败！");
//				关闭
		statement.close();
		connection.close();
	}
	/**
	 * 增加点击量
	 * @param RId
	 * @throws SQLException 
	 */
	public void addRkick(Integer RId) throws SQLException {
		Integer k=this.findRecruitmentObj(RId).getRkick()+1;
		String sql="update recruitment set Rkick="+k+" where RId="+RId;
		System.out.println("增加招聘信息点击量的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//修改
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("加招聘信息点击量成功！");
		else System.out.println("加招聘信息点击量失败！");
//				关闭
		statement.close();
		connection.close();
	}
	
	
	
//test
	/**
	 * 根据企业ID找招聘信息
	 * @param EId
	 * @return
	 * @throws SQLException
	 */
	public List<Recruitment> searchByEIdGroupBy(String EId) throws SQLException{
//		String sql="select * from recruitment where EId='"+EId+"' group by JId";
		String sql="select * from recruitment where EId='"+EId+"' and Rstate=0";
		System.out.println("根据企业id查询相应的招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数
		
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment=null;
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
		}
	/**
	 * 寻找某企业招聘的某岗位的相关招聘信息
	 * @param jId
	 * @param eId
	 * @return
	 * @throws SQLException 
	 */
	public List<Recruitment> searchDeliverysByJIdAndEId(int jId, String EId) throws SQLException {
		String sql="select * from recruitment where EId='"+EId+"' and JId="+jId;
		System.out.println("寻找某企业招聘的某岗位的相关招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数
		
		List<Recruitment> items= new ArrayList<>();
		Recruitment recruitment=null;
		while(rs.next()) {
			recruitment=new Recruitment();
			recruitment.setRId(rs.getInt("RId"));
			recruitment.setRContent(rs.getString("RContent"));
			recruitment.setRTime(rs.getString("RTime"));
			recruitment.setRkick(rs.getInt("Rkick"));
			recruitment.setRpicture(rs.getString("Rpicture"));
			recruitment.setRstate(rs.getInt("Rstate"));
			recruitment.setRCount(rs.getInt("RCount"));
			recruitment.setEId(rs.getString("EId"));
			recruitment.setJId(rs.getInt("JId"));
			recruitment.setSalary(rs.getString("Salary"));
			recruitment.setDegree(rs.getString("Degree"));
			recruitment.setAddress(rs.getString("Address"));
			recruitment.setRTitle(rs.getString("RTitle"));
			items.add(recruitment);
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return items;
	}
}
