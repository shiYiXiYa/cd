package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.JobName;

import org.model.Enterprise;
import org.model.Job;
import org.utils.DButils;

public class JobDao {
	/**
	 * 跟据JId查询得到一个job对象
	 * @return
	 * @throws SQLException
	 */
	public Job findJobObj(Integer JId) throws SQLException {
		String sql="select * from job where JId="+JId;
		System.out.println("根据JId查询相应的job对象的sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		Job job = new Job();
		if(rs.next()) {
			job.setJId(rs.getInt("JId"));
			job.setJname(rs.getString("Jname"));
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return job;
	}
	
	/**
	 * 跟据Jname查询得到一个job对象
	 * @return
	 * @throws SQLException
	 */
	public Job findJobObj(String Jname) throws SQLException {
		String sql="select * from job where Jname='"+Jname+"'";
		System.out.println("根据JId查询相应的job对象的sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数据
		Job job =null;
		if(rs.next()) {
			job=new Job();
			job.setJId(rs.getInt("JId"));
			job.setJname(rs.getString("Jname"));
		}
		//关闭
		rs.close();
		statement.close();
		connection.close();
		return job;
	}
	/**
	 * @throws SQLException 
	 * 
	 */
	public List<Job> queryJobsToOptions(String EId) throws SQLException{
		String sql=" select * from job where JId in(select JId from recruitment where EId='"+EId+"' and Rstate=0 group by JId)";
		System.out.println("根据企业id查询相应的招聘信息列表sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		//数据查询
		ResultSet rs = statement.executeQuery();
		//封装数
		List<Job> jobs=new ArrayList<>();
		Job job =null;
		while(rs.next()) {
			job=new Job();
			job.setJId(rs.getInt("JId"));
			job.setJname(rs.getString("Jname"));
			jobs.add(job);
		}
		return jobs;
	}
	/**
	 * 插入新工作：JName
	 * @param job
	 * @throws SQLException
	 */
	public void insertJob(Job job) throws SQLException {
		String sql="insert into job(Jname) values('"+job.getJname()+"')";
		System.out.println("插入新job的sql语句:"+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//插入
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入成功！");
		else System.out.println("插入失败！");
		//关闭
		statement.close();
		connection.close();
	}
}
