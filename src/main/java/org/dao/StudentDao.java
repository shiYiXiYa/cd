package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.model.Student;
import org.utils.DButils;

public class StudentDao {
	/**
	 * 根据ID查询得到一个学生对象
	 * @param SId
	 * @return
	 * @throws SQLException
	 */
	public Student findStudentObj (String SId) throws SQLException {
	String sql="select * from student where SId=?";
	System.out.println("通过学号查找到一个学生的sql语句："+sql);
	//第一步数据库连接
	try {
		Class.forName(DButils.driveName);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
	//获得数据库操作对象
	PreparedStatement statement=connection.prepareStatement(sql);
	statement.setString(1, SId);
	//查询
	ResultSet rs = statement.executeQuery();
	Student s=null;
	if(rs.next()) {
		s=new Student();
		s.setSId(rs.getString("SId"));
		s.setSName(rs.getString("SName"));	
		s.setSNickname(rs.getString("SNickname"));
		s.setSAge(rs.getInt("SAge"));
		s.setSPwd(rs.getString("SPwd"));
		s.setDName(rs.getString("DName"));
		s.setSPhone(rs.getString("SPhone"));
		s.setSstate(rs.getInt("Sstate"));
		s.setSSc(rs.getString("SSc"));
		s.setSsex(rs.getString("Ssex"));
		s.setSSp(rs.getString("SSp"));
		s.setSImg(rs.getString("SImg"));
		}
	//关闭连接
	rs.close();
	statement.close();
	connection.close();
	return s;
	}
	/**
	 * 更新学生头像
	 * @param s
	 * @throws SQLException
	 */
	public void changeStudentImg(Student s) throws SQLException {
		String sql="update student set SImg='"+s.getSImg()+"' where SId='"+s.getSId()+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新学生头像成功！");
		else System.out.println("更新学生头像失败！");
		statement.close();
		connection.close();
	}
	/**
	 * 学生用户自己可修改自己的昵称和电话号码 flag=1
	 * 管理员修改学生信息 可修改其专业和状态  flag=2
	 * 学生用户自己可修改自己的密码		  flag=3
	 * @param s
	 * @throws SQLException
	 */
	public void changeStudentInfo(Student s,Integer flag) throws SQLException {
		String sql;
		if(flag==1) 
			sql="update student set SNickname='"+s.getSNickname()+"',"+"SPhone='"+s.getSPhone()+"' where SId='"+s.getSId()+"'";
		else if(flag==2)
			sql="update student set DName='"+s.getDName()+"',"+"Sstate="+s.getSstate()+" where SId='"+s.getSId()+"'";
		else {
			sql="update student set SPwd='"+s.getSPwd()+"' where SId='"+s.getSId()+"'";
		}
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新成功！");
		else System.out.println("更新失败！");
		statement.close();
		connection.close();
	}
	/**
	 * 多条件查询得到学生列表
	 * @param student
	 * @return
	 * @throws SQLException
	 */
	public List<Student> searchStudents(Student student) throws SQLException{
		//多条件查询
		String sql="select * from student where SId='"+student.getSId()+"' or SName='"+student.getSName()+"' or SNickname='"+student.getSNickname()+"' or DName='"+student.getDName()+"' or SSc='"+student.getSSc()+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		System.out.println(sql);
		//查询
		ResultSet rs = statement.executeQuery();
		List<Student> list = new ArrayList<>();
		while(rs.next()) {
			Student s=new Student();
			s.setSId(rs.getString("SId"));
			s.setSName(rs.getString("SName"));	
			s.setSNickname(rs.getString("SNickname"));
			s.setSAge(rs.getInt("SAge"));
			s.setSPwd(rs.getString("SPwd"));
			s.setDName(rs.getString("DName"));
			s.setSPhone(rs.getString("SPhone"));
			s.setSstate(rs.getInt("Sstate"));
			s.setSSc(rs.getString("SSc"));
			s.setSsex(rs.getString("Ssex"));
			list.add(s);
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return list;
	}
	
	public List<Student> searchBySstate(Integer sInteger) throws SQLException{
		String sql="select * from student where Sstate="+sInteger;
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		System.out.println(sql);
		//查询
		ResultSet rs = statement.executeQuery();
		List<Student> list = new ArrayList<>();
		while(rs.next()) {
			Student s=new Student();
			s.setSId(rs.getString("SId"));
			s.setSName(rs.getString("SName"));	
			s.setSNickname(rs.getString("SNickname"));
			s.setSAge(rs.getInt("SAge"));
			s.setSPwd(rs.getString("SPwd"));
			s.setDName(rs.getString("DName"));
			s.setSPhone(rs.getString("SPhone"));
			s.setSstate(rs.getInt("Sstate"));
			s.setSSc(rs.getString("SSc"));
			s.setSsex(rs.getString("Ssex"));
			list.add(s);
			}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return list;
	}
	/**
	 * 添加学生
//	 * 如何批量导入学生？
	 * @param student
	 * @throws SQLException
	 */
	public void insertStudent(Student student) throws SQLException {
		String sql="insert into student (SId,SName,SNickname,SAge,DName,SSc,Ssex) values('"+student.getSId()+"','"+student.getSName()+"','"+student.getSNickname()+"',"+student.getSAge()+",'"+student.getDName()+"','"+student.getSSc()+"','"+student.getSsex()+"')";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("插入学生成功！");
		else System.out.println("插入学生失败！");
//		关闭
		statement.close();
		connection.close();
	}
}
	