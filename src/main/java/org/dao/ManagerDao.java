package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.model.Manager;
import org.model.Student;
import org.utils.DButils;

public class ManagerDao {
	public Manager findByPhone(String p) throws SQLException {
		String sql="select * from manager where MPhone='"+p+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		ResultSet rs=statement.executeQuery();
		//封装数据
		Manager manager = new Manager();
		if(rs.next()) {
			manager.setMId(rs.getInt("MId"));
			manager.setMImg(rs.getString("MImg"));
			manager.setMName(rs.getString("MName"));
			manager.setMPhone(rs.getString("MPhone"));
			manager.setMPwd(rs.getString("MPwd"));
		}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return manager;
	}
	/**
	 * 更改管理员基本信息
	 * MName  MPhone 
	 * @param mananger
	 * @throws SQLException 
	 */
	public void changeManagerInfo(Manager mananger) throws SQLException {
		String sql="update manager set MName='"+mananger.getMName()+"',MPhone='"+mananger.getMPhone()+"'";
		System.out.println("管理员更改自身名字和电话的sql语句："+sql);
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//修改
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更改成功！");
		else System.out.println("更改失败！");
//						关闭
		statement.close();
		connection.close();
	}
	/**
	 * 更换头像
	 * @param m
	 * @throws SQLException
	 */
	public void changeManagerImg(Manager m) throws SQLException {
		String sql="update manager set MImg='"+m.getMImg()+"' where MId="+m.getMId();
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		Statement statement=connection.createStatement();
		//查询
		System.out.println(sql);
		int rs = statement.executeUpdate(sql);
		if(rs==1) System.out.println("更新管理员头像成功！");
		else System.out.println("更新管理员头像失败！");
	}
	
//test
	public Manager findByMid(String p) throws SQLException {
		String sql="select * from manager where MId='"+p+"'";
		//第一步数据库连接
		try {
			Class.forName(DButils.driveName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection=DriverManager.getConnection(DButils.url, DButils.name,DButils. password);
		//获得数据库操作对象
		PreparedStatement statement=connection.prepareStatement(sql);
		ResultSet rs=statement.executeQuery();
		//封装数据
		Manager manager = new Manager();
		if(rs.next()) {
			manager.setMId(rs.getInt("MId"));
			manager.setMImg(rs.getString("MImg"));
			manager.setMName(rs.getString("MName"));
			manager.setMPhone(rs.getString("MPhone"));
			manager.setMPwd(rs.getString("MPwd"));
		}
		//关闭连接
		rs.close();
		statement.close();
		connection.close();
		return manager;
	}
}
