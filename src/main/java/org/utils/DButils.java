package org.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DButils {
	 // 加载数据库驱动
    public static String driveName = "com.mysql.cj.jdbc.Driver";
    // 数据库连接地址
    public static String url = "jdbc:mysql://8.130.31.18:3306/cd?useUnicode=yes&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true";
    // 数据库名称
    public static String name = "root";
    // 数据库密码
    public static String password = "2845597639Yyqx.";
    // 存储连接数据库
    public static Connection connection = null;

    public static void main(String[] args) {

        // 1、加载驱动
        try {
            System.out.println("开始加载驱动");
            Class.forName(driveName);
            System.out.println("加载成功");
        } catch (Exception e) {
            System.out.println(e);
        }

        // 2、连接数据库
        try {
            System.out.println("开始连接！！");
            connection = DriverManager.getConnection(url, name, password);
            System.out.println("连接成功！！");
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            System.out.println("开始执行！！");
            // 3、获取数据库语句操作对象
            Statement stmt = connection.createStatement();
            // 4、执行SQL语句
            String sql = "select SName,SId from student limit 2,3";
            // 5、处理查询结果
            ResultSet rs = stmt.executeQuery(sql);
            // 5.1、遍历输出查询结果
            while (rs.next()) {
                String SName = rs.getString("SName");
                String SId= rs.getString("SId");
                System.out.println(SName+ "," + SId);
            }
            // 5、增删改操作
            //int i = stmt.executeUpdate(sql);
            // 6、释放资源
            rs.close();
            stmt.close();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
       

    }
}
