package org.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class Dynamic {
	private Integer DId;//int NOT NULL AUTO_INCREMENT,
	private String DContent;//text NOT NULL,
	private String DTime;//timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	private String SId;//char(11) DEFAULT NULL,
	private String DTitle;//varchar(30) NOT NULL COMMENT '��̬����',
	private Student student;
	public Integer getDId() {
		return DId;
	}
	public void setDId(Integer dId) {
		DId = dId;
	}
	public String getDContent() {
		return DContent;
	}
	public void setDContent(String dContent) {
		DContent = dContent;
	}
	public String getDTime() throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formatTime=df.format(df.parse(DTime));
		return formatTime;
	}
	public void setDTime(String dTime) {
		DTime = dTime;
	}
	public String getSId() {
		return SId;
	}
	public void setSId(String sId) {
		SId = sId;
	}
	public String getDTitle() {
		return DTitle;
	}
	public void setDTitle(String dTitle) {
		DTitle = dTitle;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
}
