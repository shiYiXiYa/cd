package org.model;

public class Student {
	  private String SId;//char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private String SName;//varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private String SNickname;//varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private int SAge;//int DEFAULT NULL,
	  private String SPwd;//varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private String DName;//int NOT NULL,
	  private String SPhone;//char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private String SSc;//varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  private int Sstate;//int NOT NULL,
	  private String Ssex;//char(1) NOT NULL,
	  private String SSp;//varchar(20) DEFAULT NULL COMMENT 'רҵ',
	  private String SImg;
	
		public String getSImg() {
			return SImg;
		}
		public void setSImg(String sImg) {
			SImg = sImg;
		}
		public String getSId() {
			return SId;
		}
		public void setSId(String sId) {
			SId = sId;
		}
		public String getSName() {
			return SName;
		}
		public void setSName(String sName) {
			SName = sName;
		}
		public String getSNickname() {
			return SNickname;
		}
		public void setSNickname(String sNickname) {
			SNickname = sNickname;
		}
		public int getSAge() {
			return SAge;
		}
		public void setSAge(int sAge) {
			SAge = sAge;
		}
		public String getSPwd() {
			return SPwd;
		}
		public void setSPwd(String sPwd) {
			SPwd = sPwd;
		}
		
		public String getDName() {
			return DName;
		}
		public void setDName(String dName) {
			DName = dName;
		}
		public String getSPhone() {
			return SPhone;
		}
		public void setSPhone(String sPhone) {
			SPhone = sPhone;
		}
		public String getSSc() {
			return SSc;
		}
		public void setSSc(String sSc) {
			SSc = sSc;
		}
		public int getSstate() {
			return Sstate;
		}
		public void setSstate(int sstate) {
			Sstate = sstate;
		}
		public String getSsex() {
			return Ssex;
		}
		public void setSsex(String ssex) {
			Ssex = ssex;
		}
		public String getSSp() {
			return SSp;
		}
		public void setSSp(String sSp) {
			SSp = sSp;
		}
		  
	}
