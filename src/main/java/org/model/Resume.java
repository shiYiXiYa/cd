package org.model;

public class Resume {
	 private Integer RsId; //int NOT NULL AUTO_INCREMENT COMMENT '简历编号',
	  private String Revaluation; //varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '自我评价',
	  private String Rsphone; //char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '电话',
	  private String Rsexper; //varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '实习经历',
	  private String SId; //char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
	  private String Jname; //int DEFAULT NULL,
	  private String RBirth; //varchar(255) NOT NULL COMMENT '出生年月',
	  private String REducation; //varchar(255) DEFAULT NULL COMMENT '学历',
	  private String RStudyTime; //varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '就读时间',
	  private String RsImg; //varchar(255) DEFAULT NULL COMMENT '证件照',
	  private String RAddress; //varchar(255) NOT NULL DEFAULT '无' COMMENT '现居地',
	  private String REmail; //varchar(255) NOT NULL COMMENT '邮箱',
	  private String RHonor; //varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '无' COMMENT '奖励荣誉',
	  private String RCertificate; //varchar(255) NOT NULL DEFAULT '无' COMMENT '证书',
	  private String RLesson;
	  public String getRLesson() {
		return RLesson;
	}
	public void setRLesson(String rLesson) {
		RLesson = rLesson;
	}
	private Student student;
	public Integer getRsId() {
		return RsId;
	}
	public void setRsId(Integer rsId) {
		RsId = rsId;
	}
	public String getRevaluation() {
		return Revaluation;
	}
	public void setRevaluation(String revaluation) {
		Revaluation = revaluation;
	}
	public String getRsphone() {
		return Rsphone;
	}
	public void setRsphone(String rsphone) {
		Rsphone = rsphone;
	}
	public String getRsexper() {
		return Rsexper;
	}
	public void setRsexper(String rsexper) {
		Rsexper = rsexper;
	}
	public String getSId() {
		return SId;
	}
	public void setSId(String sId) {
		SId = sId;
	}
	
	public String getJname() {
		return Jname;
	}
	public void setJname(String jname) {
		Jname = jname;
	}

	
	public String getRBirth() {
		return RBirth;
	}
	public void setRBirth(String rBirth) {
		RBirth = rBirth;
	}
	public String getREducation() {
		return REducation;
	}
	public void setREducation(String rEducation) {
		REducation = rEducation;
	}
	public String getRStudyTime() {
		return RStudyTime;
	}
	public void setRStudyTime(String rStudyTime) {
		RStudyTime = rStudyTime;
	}
	public String getRsImg() {
		return RsImg;
	}
	public void setRsImg(String rsImg) {
		RsImg = rsImg;
	}
	public String getRAddress() {
		return RAddress;
	}
	public void setRAddress(String rAddress) {
		RAddress = rAddress;
	}
	public String getREmail() {
		return REmail;
	}
	public void setREmail(String rEmail) {
		REmail = rEmail;
	}
	public String getRHonor() {
		return RHonor;
	}
	public void setRHonor(String rHonor) {
		RHonor = rHonor;
	}
	public String getRCertificate() {
		return RCertificate;
	}
	public void setRCertificate(String rCertificate) {
		RCertificate = rCertificate;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
}
