package org.model;

public class Enterprise {
	  private String EId; //char(13) NOT NULL COMMENT '企业号',
	  private String EPwd; //varchar(20) NOT NULL COMMENT '密码',
	  private String ECur; //text COMMENT '企业简介',
	  private String EName; //varchar(80) NOT NULL COMMENT '企业名称',
	  private String EPrinName; //varchar(20) NOT NULL COMMENT '负责人姓名',
	  private String EPosition; //varchar(20) NOT NULL COMMENT '负责人职位',
	  private Integer Estate; //int NOT NULL DEFAULT '0' COMMENT '状态',
	  private String EImg; //varchar(255) NOT NULL DEFAULT '帐号.png' COMMENT '头像',
	public String getEId() {
		return EId;
	}
	public void setEId(String eId) {
		EId = eId;
	}
	public String getEPwd() {
		return EPwd;
	}
	public void setEPwd(String ePwd) {
		EPwd = ePwd;
	}
	public String getECur() {
		return ECur;
	}
	public void setECur(String eCur) {
		ECur = eCur;
	}
	public String getEName() {
		return EName;
	}
	public void setEName(String eName) {
		EName = eName;
	}
	public String getEPrinName() {
		return EPrinName;
	}
	public void setEPrinName(String ePrinName) {
		EPrinName = ePrinName;
	}
	public String getEPosition() {
		return EPosition;
	}
	public void setEPosition(String ePosition) {
		EPosition = ePosition;
	}
	public Integer getEstate() {
		return Estate;
	}
	public void setEstate(Integer estate) {
		Estate = estate;
	}
	public String getEImg() {
		return EImg;
	}
	public void setEImg(String eImg) {
		EImg = eImg;
	}
}
