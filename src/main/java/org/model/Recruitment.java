package org.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Recruitment {
	  private Integer RId; //int NOT NULL AUTO_INCREMENT,
	  private String RContent; //text NOT NULL,
	  private String RTime; //timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  private Integer Rkick; //int NOT NULL DEFAULT '0',
	  private Integer Rstate; //int NOT NULL DEFAULT '0',
	  private String EId; //char(13) DEFAULT NULL,
	  private Integer JId; //int DEFAULT NULL,
	  private String Salary; //varchar(20) NOT NULL COMMENT '薪资',
	  private String Degree; //varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '学历',
	  private String Address; //varchar(20) DEFAULT NULL COMMENT '工作地址',
	  private String RTitle; //varchar(255) DEFAULT NULL COMMENT '招聘信息主题',
	  private Integer RCount; //int DEFAULT NULL COMMENT '招聘人数',
	  private String Rpicture;
	  private Job job;
	  private Enterprise enterprise;
	  public String getRpicture() {
		return Rpicture;
	}
	public void setRpicture(String rpicture) {
		Rpicture = rpicture;
	}
	
	  
	public Integer getRId() {
		return RId;
	}
	public void setRId(Integer rId) {
		RId = rId;
	}
	public String getRContent() {
		return RContent;
	}
	public void setRContent(String rContent) {
		RContent = rContent;
	}
	public String getRTime() throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formatTime=df.format(df.parse(RTime));
		return formatTime;
	}
	public void setRTime(String rTime) {
		RTime = rTime;
	}
	public Integer getRkick() {
		return Rkick;
	}
	public void setRkick(Integer rkick) {
		Rkick = rkick;
	}
	public Integer getRstate() {
		return Rstate;
	}
	public void setRstate(Integer rstate) {
		Rstate = rstate;
	}
	public String getEId() {
		return EId;
	}
	public void setEId(String eId) {
		EId = eId;
	}
	public Integer getJId() {
		return JId;
	}
	public void setJId(Integer jId) {
		JId = jId;
	}
	public String getSalary() {
		return Salary;
	}
	public void setSalary(String salary) {
		Salary = salary;
	}
	public String getDegree() {
		return Degree;
	}
	public void setDegree(String degree) {
		Degree = degree;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getRTitle() {
		return RTitle;
	}
	public void setRTitle(String rTitle) {
		RTitle = rTitle;
	}
	public Integer getRCount() {
		return RCount;
	}
	public void setRCount(Integer rCount) {
		RCount = rCount;
	}
	public Enterprise getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}
	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
}
