package org.model;

public class Delivery {
	 private String SId; //char(11) NOT NULL,
	 private Integer RId; //int NOT NULL,
	 private Integer Astate; //int NOT NULL DEFAULT '0',
	 private Student student;
	 private Recruitment recruitment;
	public String getSId() {
		return SId;
	}
	public void setSId(String sId) {
		SId = sId;
	}
	public Integer getRId() {
		return RId;
	}
	public void setRId(Integer rId) {
		RId = rId;
	}
	public Integer getAstate() {
		return Astate;
	}
	public void setAstate(Integer astate) {
		Astate = astate;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Recruitment getRecruitment() {
		return recruitment;
	}
	public void setRecruitment(Recruitment recruitment) {
		this.recruitment = recruitment;
	}
	
}
