package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DeliveryDao;
import org.dao.EnterpriseDao;
import org.dao.RecruitmentDao;
import org.dao.ResumeDao;
import org.model.Delivery;
import org.model.Enterprise;
import org.model.Recruitment;
import org.model.Resume;
@WebServlet(urlPatterns = "/delivery")
public class DeliveryController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		System.out.println("/delivery的action:"+action);
		if("delivery".equals(action)) {
			this.delivery(request, response);
		}else if("check".equals(action)) {
			this.check(request, response);
		}else if("record".equals(action)) {
			this.record(request, response);
		}
	}
	/**
	 * 搜索该学生的投递记录
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void record(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DeliveryDao deliveryDao = new DeliveryDao();
		RecruitmentDao recruitmentDao = new RecruitmentDao();
		EnterpriseDao enterpriseDao = new EnterpriseDao();
		String SId = request.getParameter("SId");
		try {
			List<Delivery> deliverys = deliveryDao.findDelivery(SId);
			if(deliverys.size()!=0) {
				for(Delivery item:deliverys) {
					Recruitment recruitment = recruitmentDao.findRecruitmentObj(item.getRId());
					Enterprise enterprise = enterpriseDao.findByEnterpriseObj(recruitment.getEId());
					recruitment.setEnterprise(enterprise);
					item.setRecruitment(recruitment);
//					item.getRecruitment().getRTitle();
//					item.getRecruitment().getRCount();
//					item.getAstate();//1——>录取
//					item.getRId();
//					item.getRecruitment().getEnterprise().getEName();
				}
				HttpSession session = request.getSession();
				session.setAttribute("deliveryRecord", deliverys);
				System.out.println("deliverys.size()"+deliverys.size());
				response.sendRedirect(request.getContextPath()+"/views/delivery-record.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("record_is_null", "你还没有投递任何简历，快去看看吧！");
		request.getRequestDispatcher("/views/delivery-record.jsp").forward(request, response);
	}
	/**
	 * 检查该用户是否已经投递过该招聘信息
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void check(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String SId = request.getParameter("SId");
		int RId = Integer.parseInt(request.getParameter("RId"));
		DeliveryDao deliveryDao = new DeliveryDao();
		try {
			Delivery delivery = deliveryDao.findDelivery(RId, SId);
			if(delivery!=null) {
				request.setAttribute("exist", "投递简历");
				request.setAttribute("deliveryFlag", "1");				
				request.getRequestDispatcher("/views/recruitment-display.jsp").forward(request, response);
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("exist", "投递简历");
		request.getRequestDispatcher("/views/recruitment-display.jsp").forward(request, response);
		
	}

	/**
	 * 记录下新的投递记录
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void delivery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String SId=request.getParameter("SId");
			int RId = Integer.parseInt(request.getParameter("RId"));
			ResumeDao resumeDao = new ResumeDao();
			Resume resume = resumeDao.findResumeObj(SId);
			if(resume==null) {
				request.setAttribute("resume_is_null", "没有建立简历");
				request.getRequestDispatcher("/views/recruitment-display.jsp").forward(request, response);
				return;
			}
			DeliveryDao deliveryDao = new DeliveryDao();
			Delivery delivery = new Delivery();
			delivery.setSId(SId);
			delivery.setRId(RId);
			deliveryDao.insertDelivery(delivery);
		} catch (SQLException e) {
			System.out.println("捕捉到了错误e："+e);
		}
			request.setAttribute("success", "已成功投递");
			request.removeAttribute("resume_is_null");
			request.getRequestDispatcher("/views/recruitment-display.jsp").forward(request, response);
	}

}
