package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DynamicDao;
import org.dao.EnterpriseDao;
import org.dao.JobDao;
import org.dao.RecruitmentDao;
import org.model.Dynamic;
import org.model.Enterprise;
import org.model.Job;
import org.model.Recruitment;

import com.mysql.cj.Session;
@WebServlet(urlPatterns = "/enterprise")
public class EnterController extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		System.out.println(action);
		if("login".equals(action)){
				this.login(request, response);
		}else if("register".equals(action)) {
			this.register(request,response);
		}
	}
	
	/**
	 * 注册
	 * @param request
	 * @param response
	 */
	private void register(HttpServletRequest request, HttpServletResponse response) {
		try {
			String EId=request.getParameter("EId");
			String name=request.getParameter("Ename");
			Enterprise enterprise2 = new Enterprise();
			enterprise2.setEId(EId);
			enterprise2.setEName(name);
			enterprise2.setEstate(1);
			EnterpriseDao enterpriseDao = new EnterpriseDao();
			enterpriseDao.insertEnterprise(enterprise2);
			enterpriseDao.changeEnterpriseInfo(enterprise2, 2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 企业用户登录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void login(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {
		try {
			String EId=request.getParameter("eid");
			String Epwd=request.getParameter("Epwd");
			EnterpriseDao enterpriseDao = new EnterpriseDao();
			Enterprise enterprise=enterpriseDao.findByEnterpriseObj(EId);
			
			if(enterprise!=null&&Epwd.equals(enterprise.getEPwd())) {
				//登陆成功，设置数据在会话里面，并且跳转首页
				HttpSession session=request.getSession();
				session.setAttribute("enterprise", enterprise);
				response.sendRedirect(request.getContextPath()+"/views/index.jsp");
				return;
		}} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("enter_login_msg", "用户登录失败，请重新输入账号和密码");
		request.getRequestDispatcher("/views/login.jsp").forward(request, response);
	}
}
