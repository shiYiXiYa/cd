package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DynamicDao;
import org.dao.EnterpriseDao;
import org.dao.JobDao;
import org.dao.RecruitmentDao;
import org.dao.StudentDao;
import org.model.Dynamic;
import org.model.Enterprise;
import org.model.Recruitment;
import org.model.Student;
import org.service.StudentService;
import org.service.imple.StudentServiceImple;


@WebServlet(urlPatterns = "/student")
public class StudentController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
//		System.out.println(action);
		if("login".equals(action)){
			this.studentlogin(request, response);
		}else if("resume".equals(action)) {
			this.resume(request, response);
		}
	}
	/**
	 * 得到该学生的部分简历里的内容
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
private void resume(HttpServletRequest request, HttpServletResponse response) throws IOException {
	try {
		String sIdString = request.getParameter("SId");
		StudentDao studentDao = new StudentDao();
		Student findStudentObj = studentDao.findStudentObj(sIdString);
		if (findStudentObj!=null) {
			HttpSession session = request.getSession();
			session.setAttribute("findStudentObj", findStudentObj);
			response.sendRedirect(request.getContextPath()+"/views/resume-update.jsp");
			return;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	System.out.println("Error");
	}
	//	HttpServletRequest request, HttpServletResponse response
//学生登录处理
	private void studentlogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		String sid = request.getParameter("sid");
		String spwd = request.getParameter("spwd");
		StudentDao studentDao = new StudentDao();
		Student student = studentDao.findStudentObj(sid);
//		System.out.println(sid+"   "+spwd+"   "+student.getSImg());
		
		if(student!=null&&spwd.equals(student.getSPwd())) {
			HttpSession session=request.getSession();
			session.setAttribute("student", student);
			response.sendRedirect(request.getContextPath()+"/views/index.jsp");
			return;
			} 
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		request.setAttribute("student_login_msg", "用户登录失败，请重新输入账号和密码");
		request.getRequestDispatcher("/views/login.jsp").forward(request, response);
		}
	}


