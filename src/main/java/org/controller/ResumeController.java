package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DeliveryDao;
import org.dao.EnterpriseDao;
import org.dao.JobDao;
import org.dao.RecruitmentDao;
import org.dao.ResumeDao;
import org.dao.StudentDao;
import org.model.Delivery;
import org.model.Enterprise;
import org.model.Job;
import org.model.Recruitment;
import org.model.Resume;
import org.model.Student;
@WebServlet(urlPatterns = "/resume")
public class ResumeController extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException  {
		String action = request.getParameter("action");
		System.out.println("url='/resume'的action: "+action);
		if("findjob".equals(action)) {
			this.findJob(request, response);
		}else if("manager".equals(action)){
			this.manage(request,response);
		}else if("change".equals(action)) {
			this.change(request, response);
		}else if("update".equals(action)) {
			this.update(request, response);
		}else if("display".equals(action)) {
			this.display(request, response);
		}
	}
	/**
	 * 企业查看简历详情
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws SQLException 
	 */
	private void display(HttpServletRequest request, HttpServletResponse response) throws IOException  {
		try {
			String sid = request.getParameter("sid");
			StudentDao studentDao = new StudentDao();
			Student student = studentDao.findStudentObj(sid);
			ResumeDao resumeDao = new ResumeDao();
			Resume resume = resumeDao.findResumeObj(sid);
			if(resume!=null) {
				resume.setStudent(student);
				HttpSession session = request.getSession();
				session.setAttribute("resume", resume);
				response.sendRedirect(request.getContextPath()+"/views/resume-display.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("e:"+e);
			e.printStackTrace();
			return;
		}
	}
	/**
	 * 更新简历
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void update(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {	
			String sid = request.getParameter("SId");
			String img = request.getParameter("img");
			String birthday = request.getParameter("birthday");
			String Jname = request.getParameter("Jname");
			String email = request.getParameter("email");
			String degree = request.getParameter("degree");
			String address = request.getParameter("address");
			String studytime = request.getParameter("studytime");
			String phone = request.getParameter("phone");
			String lesson = request.getParameter("lesson");
			String experience = request.getParameter("experience");
			String honor = request.getParameter("honor");
			String certificate = request.getParameter("certificate");
			String evaluation = request.getParameter("evaluation");
			ResumeDao resumeDao = new ResumeDao();
			Resume resume = new Resume();
			System.out.println("lesson"+lesson);
			resume.setSId(sid);
			resume.setJname(Jname);
			resume.setRsImg(img);
			resume.setRAddress(address);
			resume.setRBirth(birthday);
			resume.setRCertificate(certificate);
			resume.setRevaluation(evaluation);
			resume.setREducation(degree);
			resume.setREmail(email);
			resume.setRBirth(birthday);
			resume.setRHonor(honor);
			resume.setRLesson(lesson);
			resume.setRsexper(experience);
			resume.setRStudyTime(studytime);
			resume.setRsphone(phone);
			resumeDao.changeResumeInfo(resume);
			Resume resume2 = resumeDao.findResumeObj(sid);
			StudentDao studentDao = new StudentDao();
			Student student = studentDao.findStudentObj(sid);
			if (resume2!=null) {
				resume2.setStudent(student);
				HttpSession session = request.getSession();
				session.setAttribute("resume", resume2);
				response.sendRedirect(request.getContextPath()+"/views/resume-display.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("e:"+e);
			e.printStackTrace();
			return;
		}
	}
	private void change(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int RId = Integer.parseInt(request.getParameter("rid"));
			String SId = request.getParameter("sid");
			int state = Integer.parseInt(request.getParameter("state"));
			DeliveryDao deliveryDao = new DeliveryDao();
			deliveryDao.changeAstate(state, SId,RId);
			this.manage(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 展示岗位对于的简历列表
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void manage(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {
		try {
			int JId = Integer.parseInt(request.getParameter("JId"));
			String EId = request.getParameter("EId");
			RecruitmentDao recruitmentDao = new RecruitmentDao();
			DeliveryDao deliveryDao = new DeliveryDao();
			JobDao jobDao = new JobDao();
			Job job = jobDao.findJobObj(JId);
			String jname = job.getJname();
			StudentDao studentDao = new StudentDao();
			List<Delivery> deliveryList = new ArrayList<>();
			List<Recruitment> recruitments = recruitmentDao.searchDeliverysByJIdAndEId(JId,EId);
			String RIdlist="(";
			System.out.println("size:"+recruitments.size());
			for(Recruitment item:recruitments) {
				RIdlist+=item.getRId()+",";
			}
			RIdlist=RIdlist.substring(0,RIdlist.length()-1);
			RIdlist+=")";
			deliveryList=deliveryDao.findDeliverysByRidString(RIdlist);
			System.out.println("deliverylisSize:"+deliveryList.size());
			for(Delivery item2:deliveryList) {
				System.out.println("学号："+item2.getSId());
				Student student = studentDao.findStudentObj(item2.getSId());
				item2.setStudent(student);
				System.out.println("加入学生");
		}
			if(deliveryList.size()!=0) {
				HttpSession session = request.getSession();
				session.setAttribute("deliveryList", deliveryList);
				System.out.println("deliveryList");
				request.setAttribute("delivery_select", jname);
				request.setAttribute("delivery_selectid", JId);
				request.getRequestDispatcher("/views/recruitment-managing.jsp").forward(request, response);
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("delivery_notice", "还没有人投递简历，再等等吧！");
		request.getRequestDispatcher("/views/recruitment-managing.jsp").forward(request, response);
	}
	/**
	 * 选出该企业的所有招聘岗位
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void findJob(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String EId = request.getParameter("EId");
			JobDao jobDao = new JobDao();
			RecruitmentDao recruitmentDao = new RecruitmentDao();
//			List<Recruitment> recruitments = recruitmentDao.searchByEIdGroupBy(EId);
			List<Job> jobs=jobDao.queryJobsToOptions(EId);
//			ArrayList<Job> joblist = new ArrayList<>();
//			for(Recruitment item:recruitments) {
//				Job job = jobDao.findJobObj(item.getJId());
//				joblist.add(job);
//			}
			if(jobs.size()!=0) {
				System.out.println("得到job list");
				HttpSession session = request.getSession();
//				session.setAttribute("joblist", joblist);
				session.setAttribute("joblistOptions", jobs);
				response.sendRedirect(request.getContextPath()+"/views/recruitment-managing.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath()+"/views/recruitment-managing.jsp");
	}
}
