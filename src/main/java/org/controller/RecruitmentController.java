package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.EnterpriseDao;
import org.dao.JobDao;
import org.dao.RecruitmentDao;
import org.model.Enterprise;
import org.model.Job;
import org.model.Recruitment;
@WebServlet(urlPatterns = "/recruitment")
public class RecruitmentController extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String action=request.getParameter("action");
		System.out.println("/recruitment action: "+action);
		if("manager".equals(action)) {
			this.manager(request,response);
		}else if("delete".equals(action)) {
			this.delete(request, response);
		}else if("insert".equals(action)) {
			this.insert(request, response);
		}else if("obtain".equals(action)) {
			this.obtain(request, response);
		}else if("obtaindetails".equals(action)) {
			this.obtaindetails(request, response);
		}
	}
	/**
	 * 转到招聘信息的展示界面
	 * 加入企业
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void obtaindetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			int RId=Integer.parseInt(request.getParameter("RId"));
			RecruitmentDao recruitmentDao = new RecruitmentDao();
			recruitmentDao.addRkick(RId);
			Recruitment recruitment;
			recruitment = recruitmentDao.findRecruitmentObj(RId);
			JobDao jobDao = new JobDao();
			Job job = jobDao.findJobObj(recruitment.getJId());
			recruitment.setJob(job);
			EnterpriseDao enterpriseDao = new EnterpriseDao();
			Enterprise enterprise = enterpriseDao.findByEnterpriseObj(recruitment.getEId());
			recruitment.setEnterprise(enterprise);
			if(recruitment!=null) {
				System.out.println("!=null");
				HttpSession session = request.getSession();
				session.setAttribute("redisplay", recruitment);
//				System.out.println(session.getAttribute("re-item").getClass());
				response.sendRedirect(request.getContextPath()+"/views/recruitment-display.jsp");
				return;
			}
		} catch (SQLException e) {
			System.out.println("捕捉到了错误："+e);
		}
	}
	/**
	 * 获得一条招聘信息的具体信息
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void obtain(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			int RId=Integer.parseInt(request.getParameter("RId"));
			RecruitmentDao recruitmentDao = new RecruitmentDao();
			Recruitment recruitment=recruitmentDao.findRecruitmentObj(RId);
			JobDao jobDao = new JobDao();
			Job job = jobDao.findJobObj(recruitment.getJId());
			recruitment.setJob(job);
			System.out.println(recruitment.getRContent());
			if(recruitment!=null) {
				System.out.println("!=null");
				HttpSession session = request.getSession();
				session.setAttribute("recruitment", recruitment);
//				System.out.println(session.getAttribute("re-item").getClass());
				response.sendRedirect(request.getContextPath()+"/views/recruitment-update.jsp");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("捕捉到了错误："+e);
		}
	}
	/**
	 * 新增招聘信息
	 * 传参EId
	 * @param request
	 * @param response
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private void insert(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		try {
		String EId=request.getParameter("EId");
		String title = request.getParameter("title");
		String degree = request.getParameter("degree");
		//需要检查job表里是否已有该工作，否则添加
		String jname = request.getParameter("job");
		int count = Integer.parseInt(request.getParameter("count"));
		String salary=request.getParameter("sa")+"-"+request.getParameter("lary");
		String address = request.getParameter("address");
		String content = request.getParameter("content");
		Recruitment recruitment=new Recruitment();
		//需要检查job表里是否已有该工作，否则添加
		JobDao jobDao = new JobDao();
		Job	job=jobDao.findJobObj(jname);
		System.out.println(job);
		if(job==null) {
			job=new Job();
			job.setJname(jname);
			jobDao.insertJob(job);
			Job	job2=jobDao.findJobObj(jname);
			recruitment.setJob(job2);
			recruitment.setJId(job2.getJId());
		}else {
			recruitment.setJob(job);
			recruitment.setJId(job.getJId());
		}
		recruitment.setEId(EId);
		recruitment.setRTitle(title);
		recruitment.setDegree(degree);
		recruitment.setSalary(salary);
		recruitment.setRCount(count);
		recruitment.setAddress(address);
		recruitment.setRContent(content);
		RecruitmentDao recruitmentDao = new RecruitmentDao();
		recruitmentDao.insertRecruitment(recruitment);
		} catch (SQLException e) {
			System.out.println("捕捉到了错误："+e);
		}
//		response.sendRedirect(request.getContextPath()+"/views/recruitment-managing.jsp");
		this.manager(request, response);
	}
	/**
	 * 删除招聘信息——改变其状态
	 * @param request
	 * @param response
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
		int RId=Integer.parseInt(request.getParameter("RId"));
		int state = Integer.parseInt(request.getParameter("state"));
		RecruitmentDao recruitmentDao = new RecruitmentDao();
		recruitmentDao.changeRstate(state,RId);
		String id=request.getParameter("EId");
		JobDao jobDao = new JobDao();
		List<Recruitment> recruitments=recruitmentDao.searchByEId(id);
		System.out.println(recruitments.size());
		for(Recruitment item:recruitments) {
			Job job=jobDao.findJobObj(item.getJId());
			item.setJob(job);
		}
		if (recruitments.size()!=0) {
				HttpSession session = request.getSession();
				session.setAttribute("recruitments", recruitments);
				response.sendRedirect(request.getContextPath()+"/views/recruitment-managing.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.getRequestDispatcher("/views/recruitment-managing.jsp").forward(request, response);
	}
	/**
	 * 招聘信息管理
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void manager(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		try {
		String id=request.getParameter("EId");
		RecruitmentDao recruitmentDao = new RecruitmentDao();
		JobDao jobDao = new JobDao();
		List<Recruitment> recruitments=recruitmentDao.searchByEId(id);
		for(Recruitment item:recruitments) {
			System.out.println(item.getRId());
			Job job=jobDao.findJobObj(item.getJId());
			item.setJob(job);
		}
			if (recruitments.size()!=0) {
				request.removeAttribute("joblist");
				HttpSession session = request.getSession();
				session.setAttribute("recruitments", recruitments);
				response.sendRedirect(request.getContextPath()+"/views/recruitment-managing.jsp");
				return;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		request.getRequestDispatcher("/views/recruitment-managing.jsp").forward(request, response);
	}
}
