package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DynamicDao;
import org.model.Dynamic;
import org.service.PageService;
import org.service.imple.PageSErviceImple;
@WebServlet(urlPatterns = "/fenye")
public class PageController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if("dynamic".equals(action)) {
			this.fenyeDynamics(request, response);
		}
	}
	
	private void fenyeDynamics(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		try {
			Integer num=6,count=0;
			Integer yemaInteger=Integer.parseInt(request.getParameter("yema"));
			System.out.println("ҳ��"+yemaInteger);
			PageService pageImplePageService=new PageSErviceImple();
			DynamicDao dynamicdao = new DynamicDao();
			Integer page1=num*(yemaInteger-1);
			System.out.println("��ѯ��ʼ��"+page1);
			List<Dynamic> dynamics =pageImplePageService.dynamicList(page1,num);
			count=dynamicdao.getCount();
//			for(Dynamic item:dynamics) {
//				System.out.println(item.getDTitle());
//			}
			HttpSession session=request.getSession();
			session.setAttribute("page_dynamics", dynamics);
			//System.out.println(session.getAttribute("page_dynamics"));
			request.setAttribute("total_pages",Math.round((Math.ceil(count/num))));
			request.setAttribute("current_page", yemaInteger);
			request.getRequestDispatcher("/views/dynamic-list.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
