package org.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.ManagerDao;
import org.model.Manager;
@WebServlet(urlPatterns = "/maneger")
public class ManagerController extends HttpServlet{
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		System.out.println(action);
		if("login".equals(action)){
				this.login(request, response);
		}
	}

	private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String mid = request.getParameter("mid");
		String mpwd=request.getParameter("mpwd");
		ManagerDao managerDao = new ManagerDao();
		try {
			Manager manager = managerDao.findByMid(mid);
			if(manager!=null&&manager.getMPwd().equals(mpwd)) {
				HttpSession session = request.getSession();
				session.setAttribute("manager", manager);
				response.sendRedirect(request.getContextPath()+"/views/manager-houtai.jsp");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
