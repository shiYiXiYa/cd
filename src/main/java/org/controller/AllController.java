package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DynamicDao;
import org.dao.JobDao;
import org.dao.RecruitmentDao;
import org.model.Dynamic;
import org.model.Recruitment;

@WebServlet(urlPatterns = "/all")
public class AllController extends HttpServlet{
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		System.out.println(action);
		if("all".equals(action)){
				this.all(request, response);
		}else if("cancel".equals(action)) {
			this.cancel(request,response);
		}
	}
	/**
	 * ע������
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void cancel(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		if(session.getAttribute("student")!=null) {
			session.removeAttribute("student");
			System.out.println("���student");
		}else {
			session.removeAttribute("enterprise");
			System.out.println("���enter");
		}
		response.sendRedirect(request.getContextPath()+"/views/login.jsp");
	}

	private void all(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		try {
		RecruitmentDao recruitmentDao = new RecruitmentDao();
		DynamicDao dynamicDao = new DynamicDao();
		JobDao jobDao = new JobDao();
		List<Recruitment> HotRecruitments;
		HotRecruitments = recruitmentDao.searchHotRecruitments();	
//		for(Recruitment item:HotRecruitments) {
//			item.setJob(jobDao.findJobObj(item.getJId()));
//		}
		List<Recruitment> newRecruitments = recruitmentDao.searchNewRecruitments();
//>start
//		for(Recruitment item:newRecruitments) {
//			item.setJob(jobDao.findJobObj(item.getJId()));
//		}
		
		List<Dynamic> fiveDynamics= dynamicDao.findDynamicsByPages(0, 5);
			HttpSession session=request.getSession();
			session.setAttribute("HotRecruitments",HotRecruitments);
			session.setAttribute("newRecruitments",newRecruitments);
			session.setAttribute("fiveDynamics",fiveDynamics);
			response.sendRedirect(request.getContextPath()+"/views/index.jsp");
			return;
			 
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
