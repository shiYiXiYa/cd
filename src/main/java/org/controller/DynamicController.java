package org.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dao.DynamicDao;
import org.dao.StudentDao;
import org.model.Dynamic;
import org.model.Student;
@WebServlet(urlPatterns = "/dynamic")
public class DynamicController extends  HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		System.out.println("dynamic:"+action);
		if ("release".equals(action)) {
			this.release(request,response);
		}else if("search".equals(action)) {
			this.search(request,response);
		}else if("display".equals(action)) {
			this.display(request, response);
		}
	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	private void display(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		DynamicDao dynamicDao = new DynamicDao();
		Student student = new Student();
		StudentDao studentDao = new StudentDao();
		Dynamic dynamic=null;
 		try {
			dynamic=dynamicDao.finddynamicObj(Integer.parseInt(id));
			if(dynamic!=null) {
				HttpSession session = request.getSession();
				student=studentDao.findStudentObj(dynamic.getSId());
				dynamic.setStudent(student);
				session.setAttribute("dynamic", dynamic);
				response.sendRedirect(request.getContextPath()+"/views/dynamic-display.jsp");
				return;
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		System.out.println("出错！");
		
	}
	/**
	 * 多条件搜索
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void search(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String searchString=request.getParameter("search");
		try {// TODO Auto-generated method stub
		System.out.println("searchString:"+searchString);
		//根据学生用户昵称：
		StudentDao studentDao = new StudentDao();
		Student student = new Student();
		student.setSId(searchString);
		student.setSName(searchString);
		student.setSNickname(searchString);
		student.setDName(searchString);
		student.setSSc(searchString);
		//根据昵称、标题、内容
		DynamicDao dynamicDao = new DynamicDao();
		Dynamic dynamic =new Dynamic();
		dynamic.setDContent(searchString);
		dynamic.setDTitle(searchString);
		dynamic.setStudent(student);
		List<Dynamic> dynamics = dynamicDao.searchDynamics(dynamic);
		
		
		if (dynamics.size()!=0) {
			for(Dynamic item:dynamics){
				Student student2=studentDao.findStudentObj(item.getSId());
				item.setStudent(student2);
			}
			HttpSession session = request.getSession();
			session.setAttribute("searchDynamics",dynamics);
		}
		} catch (SQLException e) {
			System.out.println("捕捉错误"+e);
		}
		request.setAttribute("searchString", searchString);
		request.getRequestDispatcher("/views/dynamic-search.jsp").forward(request, response);
	}
	/***
	 * 发布新动态
	 * @throws IOException 
	 */

	private void release(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
		String title=request.getParameter("title");
		String id=request.getParameter("SId");
		String content= request.getParameter("content");
		Dynamic dynamic = new	Dynamic();
		dynamic.setDTitle(title);
		dynamic.setDContent(content);
		dynamic.setSId(id);
		DynamicDao dynamicDao = new DynamicDao();
		dynamicDao.insertDynamic(dynamic);
		System.out.println("发布新动态");
		response.sendRedirect(request.getContextPath()+"/views/dynamic-list.jsp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
