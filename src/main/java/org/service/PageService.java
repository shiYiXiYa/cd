package org.service;

import java.sql.SQLException;
import java.util.List;

import org.model.Dynamic;

public interface PageService {
	/**
	 * 分页查找 学生动态
	 * @throws SQLException 
	 */
	List<Dynamic> dynamicList(Integer page1,Integer page2) throws SQLException;
	//记录相关动态有多少条是不需要额外处理的
}
