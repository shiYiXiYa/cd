package org.service.imple;

import java.sql.SQLException;
import java.util.List;

import org.dao.DynamicDao;
import org.dao.StudentDao;
import org.model.Dynamic;
import org.model.Student;
import org.service.PageService;

public class PageSErviceImple implements PageService{

	@Override
	public List<Dynamic> dynamicList(Integer page1, Integer page2) throws SQLException {
		DynamicDao dynamicdao = new DynamicDao();
//		StudentDao studentdao = new StudentDao();
		List<Dynamic> dynamics=dynamicdao.findDynamicsByPages(page1, page2);
		//需要将多个表的数据封装到列表的每个dynamic里
//		for(Dynamic item:dynamics) {
//		Student student=studentdao.findStudentObj(item.getSId());
//		System.out.println(item.getDTitle());	
//			item.setStudent(student);
//		}
		return dynamics;
	}
}
