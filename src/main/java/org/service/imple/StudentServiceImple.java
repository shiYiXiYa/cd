package org.service.imple;

import java.sql.SQLException;

import org.dao.StudentDao;
import org.model.Student;
import org.service.StudentService;

public class StudentServiceImple implements StudentService
{

	@Override
	public Student login(Student student) throws SQLException {
		//第一步：获得数据库信息
		StudentDao studentdao = new StudentDao();
		Student studentObj = studentdao.findStudentObj(student.getSId());
		//System.out.println(studentObj);
		//第二步：判断是否返回null对象，以及密码是否正确
		if(studentObj!=null&&studentObj.getSPwd().equals(student.getSPwd())) {
			System.out.println("ok");
			return studentObj;
			}
		else {
			return null;
		}
		
		
	}

}
