package org.service;

import java.sql.SQLException;

import org.model.Student;

public interface StudentService {
	/**
	 * 学生登录功能
	 * return 一个学生对象
	 * @throws SQLException 
	 */
	Student login(Student student) throws SQLException;
}
